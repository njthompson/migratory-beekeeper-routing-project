\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage[margin=1.25in]{geometry}
\usepackage{amsmath}
\usepackage{graphicx}
\graphicspath{ {./images/} }
\usepackage{mathtools}
\DeclarePairedDelimiter\ceil{\lceil}{\rceil}
\DeclarePairedDelimiter\floor{\lfloor}{\rfloor}

\title{MATH7202 Project Report}
\author{Molly Parker and Nic Thompson}
\date{30 October 2020}

\begin{document}

\maketitle
\tableofcontents

\newpage
\section{Michaels Advice}

Make it longer if you want, but has to be interesting. Aim for 10 pages.\\


Wants to see that we've learned from the process.\\


If really hard, focus on why it was hard, what was wrong with the original paper, what we did to overcome problems. If we've had less success, talk about our struggles. The insight is about what we have learnt. \\

Move stuff to the appendices if we run out of room, particularly large constraint chunks.\\

\section{Introduction}
The commercial apiculture, or beekeeping, industry has two major subsidiaries: local and migratory beekeeping. Local beekeeping entails keeping hives stationary throughout all seasons, whereas migratory beekeeping develops hives for the specific purpose of pollinating crops across multiple locations. This significantly increases honey production potential by moving bee colonies to seasonally advantageous regions throughout the year.\\

Apiculture has two key focuses that define the industry; honey generated from hives, and the external effects bees have by pollinating their surrounding landscapes. The latter can significantly help local agriculture thrive, and hence forms a productive source of income for both local and migratory beekeepers, as well as positive relationships between beekeepers and local communities.\\

Migratory beekeeping is far more strategically complex than local beekeeping, since a range of production time and location decisions need to be made that can significantly promote or impede hive production. Considering this, there are a range of factors that significantly contribute to success in the migratory beekeeping industry, including:
\begin{itemize}
\item The range of different nectar sources and floral species across different regions
\item Each location’s hive carrying capacity and honey output rate
\item Each region’s flowering periods, when hive production would be optimal; note that beekeepers can choose to either arrive late or leave early to a region's flowering period
\item The trade-off between hive production across different regions and transportation costs
\item Sale price of honey products in various regions
\end{itemize}

An efficient migratory beekeeper balances the above factors into a strategic migration plan that hopes to maximise profits across the seasons. The problem that considers these factors, and optimising them for a set of beekeepers, is discussed in detail in the paper titled The Migratory Beekeeping Routing Problem: Model and an Exact Algorithm. The paper was published in the INFORMS Journal on Computing in July 2020, and is the first paper to give a name to the Migratory Beekeeping Routing Problem, or MBRP. \\


\section{Paper Summary}

The MBRP plays similarly to the Vehicle Routing Problem with time window constraints, but by construction includes a range of other interesting characteristics. The definite tasks and characteristics of the MBRP include:
\begin{itemize}
\item Maximising revenue by generating routes for different beekeepers originating from varying initial locations - refer to Figure 1 from the paper for an example of what these routes could potentially look like
\item Revenue from distinct nectar sources is not constant, and is instead influenced by timetabling and market selection
\item Optimising production time for independent beekeepers is heavily reliant on a region’s production efficiency and market prices across a route’s visiting regions
\item Beekeepers from the same origin can choose different routes, whilst those from different origins can visit the same nectar sources
\end{itemize}

\begin{figure}[h]
\includegraphics[scale=0.6]{Figure1}
\centering
\end{figure}

The MBRP can be defined as a network where each location, which is both a nectar source and a market, is represented by a node. Each node is characterised by two dimensions - region and flower species - which determine location, flowering period, environmental carrying capacity, production efficiency, and product selling prices. At each node, the key decisions that must be made are the duration of the stay at that location, the duration of honey production at that location, and how much of which types of honey products to sell at that location. Arcs connect nectar resources, each represented by a transport time and cost.\\

The problem has a massive number of variables which prohibits the use of simple linear programming. In the paper, the researchers utilised the Dantzig-Wolfe decomposition method to break down the problem into a Restricted Master Problem (RMP) and several Pricing Subproblems. \\

The RMP aims to maximise the net revenue for each beekeeper group from a given origin and following a given route. There is only a single constraint that links this RMP with the MBRP; considering this, other constraints can be partitioned into different sets where each set of constraints is relevant only to a certain origin region. This forms the basis for the Pricing Subproblem, which aims to maximise profit over all nectar locations and varieties. \\

The paper also suggests a variation on the MBRP; the non-sales MBRP, where the sales variable (considering where and when to sell honey products from each location) is removed from the problem. This variation is much easier and faster to solve, and is used to obtain initial bounds of the MBRP. This is the variation that we have mostly been focusing on up to this point in our work on the project. \\


\section{Methodology}

 The MBRP can be described as a network where beekeepers traverse nectar sources, the nodes, along arcs that carry a certain cost for travelling. This problem can therefore be considered inside a mixed integer programming context. The paper goes into detail explaining the formulation for a mixed-integer programming implementation of the MBRP solution, as well as the solution to the non-sales MBRP. However, using these formulations, issues begin to arise due to the large number of vertices and interlinked networks in the problem graph. The huge number of variables required in these formulations prevent the use of standard mixed integer programming, due to the exponential increase in computation complexity.\\

There are two clearly defined methodologies outlined in the paper. The first is the original linear programming implementation of the problem, whilst the second is the column generation approach. The techniques and concepts that comprise the linear programming implementation of the problem heavily influence how the column generation problem is constructed. \\


Note that there are two forms of indices that the variables and sets can have; the classical formulation, which is indexed by origin region and beekeeper group, and then the Dantzig-Wolfe formulation, which divides subproblems into routes that beekeeper groups from origin routes can travel. This formulation therefore drops the indices \(k \in K\) and \(m \in M_k\). 


\subsection{Problem Formulation}



The following sections will cover the column generation formulation, omitting the original linear programming methodologies. The following sets, data and variables are used throughout the subproblem and column generation procedures. \\


\textbf{Sets}
\begin{table}[h!]
    \begin{tabular}{l|l}
	\textit{I} & Nectar source locations \\
	\textit{K} & Origin regions \\
	\textit{l} & Terminus vertex \\
	\textit{G} & Nectar varieties \\
    \end{tabular}
\end{table}


\textbf{Data}
\begin{table}[h!]
    \begin{tabular}{l|l}
	\textit{$M_k$} & Beekeeper groups from origin $k \in K$ \\
	\textit{$U_i$} & Potential markets of honey products produced at nectar source $i \in I$ \\
	\textit{$Q_i$} & Environmental carrying capacity of nectar source location $i \in I$ \\
	\textit{$q_{ig}$} & Average daily yield of honey products of variety $g \in G$ per colony at nectar source $i \in I$\\
	\textit{$T_i^S$} & Start of flowering time at nectar source $i \in I$\\
	\textit{$T^E_i$} & End of flowering time at nectar source $i \in I$\\
	\textit{$d_{ij}$} & Transport distance from nectar source $i \in I$ to $j \in I$\\
	\textit{$c_{ij}$} & Transport cost from nectar source $i \in I$ to $j \in I$, \(c_{ij} = f + cd_{ij}\), where c is the transport cost \\
	& \(\;\;\;\) per mile per group, and f is a fixed cost\\
	\textit{$p_{ig}$} & Unit price of honey product produced from nectar variety $g \in G$ at market $i \in I$\\
	\textit{b} & Average number of bee colonies per group\\
	\textit{v} & Average transport speed\\
	\textit{L} & A reasonably large number\\
    \end{tabular}
\end{table}



\textbf{Variables}
\begin{align}
x_{ij} &= 
\begin{cases}
	1, & \text{beekeeper departs from i $\in I$ to j $\in I$ } \\
	0, & \text{Otherwise}
\end{cases} \\
z_{ij} &= 
\begin{cases}
	1, & \text{indicates honey produced at nectar location i $\in I$ is sold at j $\in I$ } \\
	0, & \text{Otherwise}
\end{cases} \\
T^{I}_i &= \text{Arrival time at nectar source location i $\in I$ } \\
T^{S}_i &= \text{Starting production time at nectar source location i $\in I$ } \\
T^{P}_i &= \text{Ending production time at nectar source location i $\in I$ } \\
T^{O}_i &= \text{Departure time from nectar source location i $\in I$ } \\
w_{ij} &= \text{Used to linearise the classical sign problem, namely } w_{ij} = (T^E_i - T^S_i) \times z_{ij} \geq 0 \\
sgn_i &= \begin{cases}
	1, & \text{Indicates time spent at \textit{i} $\in I$  is substantial, i.e. } \geq 0 \\
	0, & \text{Otherwise}
\end{cases}
\end{align}\\


There are several important things to note here,

\begin{itemize}
\item The variables $w_{ij}$ and $sgn_{i}$ do not formally exist in the classical formulation. They are extensions used to linearise the sign function that determines whether a beekeeper group stays to harvest at a nectar source, or are simply passing through to get to another location. This is explained better in equations 17-23 on page 7 of the paper.
\item Notice that the starting and ending production time has the same notation as the starting and ending flowering time data. This was not an issue in the original formulation, since there were also indexes on $k \in K$ and $m \in M_k$. From this point we will refer to the timing variables as \(T^{*S}_i\) and \(T^{*E}_i\) respectively. This was something that was fairly ambiguous in the paper, however appendix A2 does cover this briefly.
\end{itemize}


Accounting for this, the notation defined is used for implementing the main subproblems of the paper. To solve the RMP, we define the following additional notation,


\begin{itemize}

\item \(\Omega_{km}\). A set of all valid routes for beekeeper group $m \in M_k$ from origin region $k \in K$.

\item \(r^\gamma_k\). The net revenue for each group following the route \(\gamma\) from origin region $k \in K$.

\item \(N^\gamma_{kj} = \sum_{m \in M_k} \sum_{i \in I \cup \{k\}} b x^\gamma_{kmij} I(t_j) \). The number of colonies that follow $\gamma$ and produce at nectar source location \(j \in J\), where $I(t_j)$ is the indicator function, which is 1 if the residence time $t_j$ is greater than zero, and zero otherwise.

\item $x_{kmij} = \sum_{\gamma \in \Omega_{km}} x^\gamma_{kmij} \lambda^\gamma_{km}$. The convex combination of extreme points of solution, characteristic of a Danzig-Wolfe process. Here \(\sum_{\gamma \in \Omega_{km}} \lambda^\gamma_{km} = 1, \lambda^\gamma_{km} \geq 0.\) Furthermore coefficient \(x^\gamma_{kmij}\) for beekeeper group $ m \in M_k$, from origin region $k \in K$ is equal to one if the arc $(i, j)$ is in the route $(k, \gamma)$, and zero otherwise.

\item \(\lambda^\gamma_k = \sum^{|M_k|}_{m=1} \lambda^\gamma_{km}\). Aggregated variables that represent the number of beekeeper groups following $(k, \gamma)$.

\end{itemize}


\subsubsection{The Restricted Master Problem}
Using this formulation, the RMP aims to maximise revenue across available routes, whilst constrained against environmental capacity, and beekeepers leaving from set origin regions. This is formulated as,

\begin{align}
\max &\; \sum_{k \in K} \sum_{\gamma \in \Omega_k} r^\gamma_k \lambda^\gamma_k \\
\text{s.t.} &\; \nonumber \\
&\; \sum_{k \in K} \sum_{\gamma \in \Omega_k} \lambda_k^\gamma \sum_{m \in M_k} \sum_{i \in I \cup \{k\}} x^\gamma_{kmij} I(t_j) \leq \floor*{\frac{Q_{j}}{b}}, \; \; \forall j \in I \\
&\; \sum_{\gamma \in \Omega_k} \lambda_k^\gamma = \vert M_k \vert, \; \; \forall k \in K
\end{align}


Equation 10 represents the environmental capacity constraint, such that collectively at any nodes, the routes passing through there cannote exceed the environmental carrying capacity. Equation 11 represents the beekeeper cover constraint, such that each beekeeper group from each origin region is allocated a corresponding route.\\

The key subtlety here is that when iteratively solving the RMP, we need to make sure the RMP data is progressively updated such that we can correctly update the coefficient in equation 10. More details on the RMP's construction can be found in section 3.1 of the paper, on page 8, namely equations 24 to 27. Note that in this case, we have used the derivation in equation 27 for the environmental carrying capacity constraint. \\


The reduced cost can be calculated through the formula,

\begin{equation}
rc_k^\gamma = r_k^\gamma - \sum_{j \in I} N^\gamma_{kj} \pi_{1j} - \pi_{0k}
\end{equation}

Where $\pi_{0k}$ represent dual variables we can get from the cover constraint (11), and $\pi_{1j}$ from the environmental carrying constraint (10).


\subsubsection{The Pricing Subproblems}

On solving the RMP, the pricing subproblem can be solved using the dual variables discerned from constraints in the RMP. Since beekeeper groups are assumed to be identical, a solution to the subproblem represents a valid route for any beekeeper group travelling from that specific origin region. As we previously mentioned, we are effectively removing the dimensions of \(k \in K\) and \(m \in M\). The pricing subproblem formulation can be found in appendix A.2 of the paper, on page 2. We can express this ourselves as follows,



\begin{align}
\max b &\sum_{i \in I} \sum_{g \in G} \left( q_{ig} \sum_{j \in U_i} p_{ig} w_{ij} \right) - \sum_{i \in I \cup \{k\}} \sum_{j \in I \cup \{l\}} c_{ij} x_{ij} - \sum_{i \in I} \pi_{1i} sgn_i - \pi_{0k} \\
\text{s.t.} &\; \nonumber \\
&\; \sum_{j \in I} x_{kj} = \sum_{i \in I} x_{il} = 1 \\
& \; \sum_{i \in I \cup \{k\}} x_{ij} = \sum_{i \in I \cup \{l\}} x_{ji} \leq 1, \; \; \forall j in I \\
&\; z_{ij} \leq \sum_{i' \in I \cup \{k\}} x_{i'j}, \; \; \forall i \in I, \forall j \in U_i \\
&\; \sum_{j \in U_i} z_{ij} \leq 1, \; \; \forall i \in I \\
&\; T^{*E}_i - T^{*S}_i \leq (T^{*E}_i - T^{*S}_i) \cdot \sum_{j \in I \cup \{ l \}} x_{ij} , \; \; \forall i \in I \\
&\; T^{*E}_i - T^{*E}_j \leq L \cdot (1-z_{ij}), \; \; \forall i \in I, \forall j \in U_i \\
&\; T^{O}_i + \ceil*{d_{ij} / v} - L(1 - x_{ij} \leq T^{I}_j , \; \; \forall i \in I \cup \{k\}, \forall j \in U_i \\
&\; T^{S}_{i} \leq T^{*S}_{i} \leq T^{*E}_{i} \leq T^{E}_{i}, \; \; \forall i \in I \\
&\; w_{ij} \geq t_i + (T^E_i - T^S_i) z_{ij} - (T^E_i - T^S_i), \; \; \forall i \in I, \forall j \in U_i \\
&\; w_{ij} \leq t_i, \; \; \forall i \in I, \forall j \in U_i \\
&\; w_{ij} \leq (T^E_i - T^S_i) z_{ij}, \; \; \forall i \in I, \forall j \in U_i 
\end{align}

Not that in this formulation, the objective function reflects reduced cost based on dual variables returned by the RMP.


\subsection{Solution Implementation}

The papers appendices outline a comprehensive way of implementing a solution to the problem. The method is divided into distinct algorithms, each responsible for contributing a smaller component to the solution, and when used collectively, provide the optimum solution to the problem. The defined algorithms are as follows, further details can be found in each subsequent part of the appendix. \\

This section will follow a paper-project style approach, where we will discuss the papers guidelines to implementing each of the fundamental algorithms, followed by details on our projects implementation. This should give a comprehensive understanding of our paper's methodology, as well as progress made under this project. For reference, the different algorithms under the main implementation include, 

\begin{itemize}
\itemsep-0.3em
\item Greedy route initialisation
\item Suboptimal solution search
\item The revised labelling algorithm
\item The algorithm for route extension
\item CG implementation for the MBRP
\end{itemize}


\subsubsection{Algorithm 1 - Greedy Route Initialisation (Appendix A.4)}

\textbf{Paper guidelines}\\

Since the column generation process requires at least some starting point to initially solve the RMP, a greedy route finding solution was proposed. To do this, the objective function of the subproblem can be restated omitting the dual variables, i.e. to only solve the non-sales MBRP. This objective becomes,

\begin{equation}
\max b \sum_{i \in I}  q_{i} p_{i} (T^{*E}_i - T^{*S}_i) - \sum_{i \in I \cup \{k\}} \sum_{j} c_{ij} x_{ij}
\end{equation}

Note that here some of the data has been re-indexed for use in the non-sales MBRP solution. More details on this re-indexing can be found in section 3.3 on page 8 of the original paper. \\

The greedy route generation algorithm works by iterating over each beekeeper group from each origin region. On each iteration, the sub-problem is solved, and accordingly the environment capacity is updated such that nodes visited by the recently added route have their capacity deducted, so further solutions have to calculate routes based on the reduced dataset. This encourages a suboptimal solution to the MBRP to be generated. \\

\textbf{Project Implementation}\\

Being the first component of the implementation, a fair amount of setup was required to get this working. Namely, this includes,

\begin{itemize}
\itemsep-0.3em
\item Loading in the required data, and setting up data structures that for use throughout the remainder of the project
\item The preparation works that involve calculating adjacency matrices, shortest paths, and time matrices
\item Implementing the subproblem, alongside the additional non-sales constraint that is employed in this section.
\end{itemize}

Originally in the project, we built out the original linear programming implementation of the MBRP. Most of the code for pre-processing data and implementing constraints used in the subproblem were done there. Therefore the main step in this section was dropping the \(k \in K\) and \(m \in M_k\) indices on the variables, and correctly setting up the subproblem. \\

This section works as per the paper's guidelines. The paper specifies that there are ways of making this quite efficient by simply using timing matrices that they define in section 3.3.1, on page 9 of the original paper. Unfortunately their terminology is relatively inconsistent between the paper and appendix, so it's quite unclear how to properly implement this in the faster way they've specified. Regardless, solving the subproblem still works, and this step was completed for this project. If given more time, looking into this speed-up would be particularly preferable. Table 1 demonstrates one of the generated routes for the 25 nectar source problem, where 0 represents the origin and 26 the terminus.


\begin{table}
  \begin{center}
    \begin{tabular}{c|c|c}
	\textbf{Node} & \textbf{Prod Start} & \textbf{Prod End} \\
	\hline
	0 & - & - \\
	21 & 30 & 52 \\
	20 & 81 & 102 \\
	7 & 163 & 196 \\
	17 & 210 & 236 \\
	15 & 242 & 273 \\
	9 & 285 & 306 \\
	10 & 310 & 341 \\
	26 & - & -
    \end{tabular}
    \caption{Solution to non-sales MBRP, unconstrained resources}
  \end{center}
\end{table}



\subsubsection{Algorithm 2 - Framework for suboptimal solution search (A.5)}

\textbf{Paper guidelines}\\

This step is used for solving the non-sales MBRP, that generates solutions that can then be used in solving the full MBRP model. The initial routes are taken from the previous section, and the following steps are looped,

\begin{itemize}
\itemsep-0.3em
\item Solve the RMP, return the dual variables
\item For all $k \in K$
\begin{itemize}
\itemsep-0.3em
\item Solve the k'th subproblem
\item If the objective value is positive, then obtain a new route and add it to the main route set of the RMP
\end{itemize}
\item If all subproblems return negative values, end the loop
\end{itemize}

In essence, each iteration involves solving the RMP, using the derived dual variables to solve each subproblem, and appending generated routes back into the main problem. The output from this algorithm ultimately determines a set of suboptimal routes for the MBRP model to then use. \\

\textbf{Project Implementation}\\

Implementing this in practice then required us to set up the RMP framework, and implement this suboptimal search process. The main issue then is setting up the RMP, since the subproblem was implemented in the previous section. The RMP wasn't in theory too hard to build out, we did need to make some simplifications though in order to get it running. \\

The paper uniquely identifies a series of different sets and data structures that are used to calculate solutions in the RMP, namely \(N^\gamma_{kj}\) and $x_{kmij}$. In our implementation rather than setting these up they were calculated on an on-demand basis. In particular we are referring to the $x^\gamma_{kmij}$ RMP data, which is pivotal in understanding the number of bee colonies producing at nodes. Initially we had this separately defined in it's own data structure, similar to how the paper explains. However, updating this variable, especially over beekeeper groups \(m \in M_k\) became particularly expensive to compute. Furthermore, because the variable is defined over various beekeeper groups \(m \in M_k\), there were a range of symmetries, causing unnecessary computation in the data structures. We countered this then by calculating the value for $x^\gamma_{kmij}$ on demand, as opposed to retaining a dataset with all known values. We also removed the dimension of \textit{m}, since for the smallest dataset example (the one we were trying to build out), this extra dimension was irrelevant. In a full implementation we would need to account for this.\\


Furthermore, it should be noted that this algorithm does not form a necessary component of the main project. It's existence is only useful in identifying a route set of suboptimal solutions that can then be passed to the column generation phase of the solution. Whilst we had this algorithm implemented, there were still errors in finding new solutions that weren't duplicates, and in the projects best interest we left this unfinished. A mock-writeup of our implementation of this \textbf{can be found in the appendix}. \\



\subsubsection{Algorithm 4 - Revise labelling algorithm (A.7)}

\textbf{Paper guidelines}\\

Before discussing the main CG algorithm for the MBRP, we need to cover the labelling and route extension algorithms. A full overview of the algorithm can be found in the original appendix. In particular, appendix A.7 defines a range of definitions and heuristics that are especially key in calculating route upper and lower revenue bounds, which is strongly relied on in the full revised labelling algorithm.\\

From a very high level, the labelling algorithm works very similar to a dynamic programming problem. It begins with An empty set of routes, indexed by \(i \in I\), i.e. the official notation is \(\Upsilon_i\). Initially, the terminus node is then added to the set. The algorithm then randomly selects a node to do, namely \(i \in I \cup \{k\}\), and identifies nodes $j \in J$ that then satisfy $a_{ji}$, where \textit{a} represents the adjacency matrix calculated in the pre-processing. From there, the route extension algorithm is then called on to prepend (this is termed append in the paper), node j to all routes in \(\Upsilon_i\). If successful, the new route is added to \(\Upsilon_j\), since the starting node will now be $j$. Once all nodes have been processed, we return all trips starting from the root node. \\


\textbf{Project Implementation}\\

The papers explanation of the revised labelling algorithm appears to work in batches, where the RMP is solved, and new routes are collectively generated, then once this is complete, all routes are simultaneously added into the model. As opposed to doing this, we restructured the method slightly to add variables into the model as they became known to the column generation process. This decision was ultimately to improve processing speeds, and reduce the amount of data that needed to be stored between processes. \\

The difficult aspect of this implementation then is adding variables into the model. The paper doesn't detail any formal way to do this, so what was done based on teachings from the course. The new route is added into the route variables using the routes total revenue as the objective. The cover constraint is then added in against the new variable with coefficient one, and for each visited nectar source in the new route \(j \in J\), the environment capacity constraint was updated with a coefficient equivalent to \(N^\gamma_{kj}\). This proved mostly successful in our implementation, i.e. this was not the cause of issues in the model (but might have been given more time towards development). \\

The other core aspect of the labelling algorithm is including dominance heuristics in the column generation process. The appendix defines a range of timing and revenue heuristics that can be used to determine upper and lower bounds for a routes total revenue. Given time constraints and difficulty in interpreting the papers explanation, this process was unfortunately largely ignored. Ignoring these steps meant we could still progress with finding a solution, but at great computational expense. 


\subsubsection{Algorithm 5 - Algorithm for appending a new nectar source (A.8)}
\textbf{Paper guidelines}\\

The route extension algorithm is crucial to the column generation process. Given a route \(\gamma_i\) (i.e. route starting at \textit{i}), the process aims to prepend a new node \(j \in I \cup \{k\}\) to the start of the route, and account for new timing changes to be made. \\

In practice, this is done by determining sources in the route that are influenced by prepending a new location to the start of the route. This influence is mathematically defined as the set of nodes \(\{v_n\}\), such that prepending \textit{j} to \(\gamma_i\) results in time readjustment only at nectar sources \(\{v_n\}\) that satisfy equation 19 (where \(v_n\) is the nth location in \(\gamma_i\)).

\begin{equation}
T^E_j + T_{jv_1} + \sum^{n-1}_{m=1} \max \left\lbrace T_{v_m v_{m+1}} , T^S_{v_{m+1}} - T^E_{v_{m}} \right\rbrace > T^{*S}_{v_n}
\end{equation}

In plain English, this condition translates to whether adding the node \textit{j} at the start of route \(\gamma_i\) will affect the production times currently in place at each location. If there there is able time to move from the new node \textit{j} after the end of the flowering period, without affecting other nodes in the route, then the list of influenced nodes will be none.\\

Importantly, if none of the nodes are influenced, we can simply prepend \textit{j} to the route. If there are multiple influenced nodes, we can then solve a linear programming programming problem that determines the new appropriate timing variables that we need to use for this new route. This model for influenced nectar sources is as follows,


\begin{align}
\max &\sum_{i \in I^F} q_i \hat{p}_i (T^{*E}_i - T^{*S}_i) - \sum_{i \in I^F} \pi_{1i} sgn_i \\
\text{s.t.} &\; \nonumber \\
&\; T^{*E}_{i} - T^{*S}_{i} \leq (T^{E}_{i} - T^{S}_{i}) \cdot sgn_i, \; \; \forall i \in I^F \\
&\; T^{S}_{i} \leq T^{*S}_{i} \leq T^{*E}_{i} \leq T^{E}_{i}, \; \; \forall i \in I^F \\
&\; T^{*E}_{v_n} + \ceil*{d_{ij} / v} \leq T^{*S}_{v_{n+1}}, \; v_n \in I^F, n < \vert I^F \vert \\
&\; T^{*E}_{v} \leq t^E
\end{align}

Where \(I^F\) is the set that of nodes comprising the route, including the new node to be appended, \textit{v} is the last influenced nectar source, and \(t^E\) is the ending production time of \textit{v}. Solving this model then yields the optimal timing for \(\gamma_j\), i.e. the new route starting at \textit{j}. \\


\textbf{Project Implementation}\\

This part of the project was the main crux of getting the problem to work. At the projects close, the route extension method was working, but a large amount of time could be further invested into making sure that the results are correct. In particular, the paper fails to describe appropriate time settings when not solving the model, and so this was assumed to be the flowering start and end for the new node \(j\). Furthermore, since the labelling algorithm moves from back to front, we need to appropriately handle timing measurements for the terminus, which is something that the paper also didn't explain. \\


Furthermore, in the implementation for the modelling component of this algorithm, we also had to include an extra constraint that wasn't mentioned in the papers appendix. This was equation 23 in the original paper (page 7), which is used to linearise the sign function, namely,

\begin{equation}
L \cdot sgn_i \geq T^{*E}_{i} - T^{*S}_{i}, \; \; \forall i \in I^F
\end{equation}

Whether this was implied in the papers appendix that we should consciously include this, we're not sure. However including it did ultimately help bring the model to correct solutions.


\subsubsection{Algorithm 3 - The CG algorithm for the MBRP (A.6)}

\textbf{Paper guidelines}\\

The final column generation algorithm builds on all the algorithms we've introduced to this point. Similar to algorithm 2 (the suboptimal search), this step works by looping through a set of steps until no new routes can be added to the problem. Within the loop, first the RMP is solved, then the dual variables and revised labelling algorithm are used to determine new routes for the problem. This continues until no new routes can be added. Then the solution to the RMP represents the optimal solution.\\

In continuation of this, the paper outlines an additional branching strategy for instances where the RMP cannot produce integer solutions. Section 3.3.3 on page 10 of the original paper goes into detail outlining this process.\\


\textbf{Project Implementation}\\


In practice, getting column generation working for this project proved incredibly difficult. There are justifications for this, collectively though it came down to errors coming from preceding sections that ultimately cause the RMP to struggle converging to a solution. \\

Namely, the main issues are that we did not enforce dominance in the labelling algorithm, and there were inconsistencies in our timing variables when applying the route extension algorithm. The first of these, enforcing dominance, immediately causes issues, since the route set grows exponentially quicker than if we had enforced dominance. The reason for omitting any form of dominance was in the interest of achieving a solution to the project, and given more time, resources, and guidance, achieving this is entirely possible. The Explanation given in the paper and appendices dives deep into theory on lower and upper bounding revenue for routes, and so quite simply, it as infeasible to get this done under this project. Without dominance then the model takes exceedingly long to even generate routes.\\


Another important issue was getting the timings of variables right. The model at the time of writing was prone to returning infeasible solutions to the timing model in the route extension algorithm. Rather than failing the model, this was simply skipped if failed, in the interest of keeping the model running. The number one cause of this is not having the timing variables right in prior routes. The paper goes to little length to explain what initial times should be used, and what times to use when no points in an existing route are influenced by an appending point. This is another aspect of the model that definitely needs fixing, and can be owed to the failure of the column generation process.






\section{Project Results}

The appendices of the paper do not provide the full results for the generated data, but the results shown on the screen are the paper’s results for select instances and beekeeper groups from the same origin regions. The first column shows the optimal objective value, the second the objective value of the linearly relaxed RMP, and the third the objective value of the unrelaxed RMP based on the generated routes before branching. The asterisk represents that the unrelaxed objective value is the same as the relaxed objective value. The values t and b.t are the total computation time and time after branching begins, respectively, in seconds. Our end goal in implementing this problem is to be able to replicate these results. \\


\section{Next Steps}

As previously mentioned, the next steps in implementing this problem are to firstly solve the non-sales MBRP using column generation, accounting for the adjacency and time matrices to improve the speed of the route generation, and once all routes have been correctly generated, to implement the restricted master problem. Finally, we will need to ensure all interim solutions are able to work together to implement and solve the complete MBRP. \\


\section{Future of the MBRP}



\section{Conclusion}

\end{document}
