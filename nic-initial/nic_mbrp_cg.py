"""
Proposal feedback,

Sorry it took me so long to get this.  A "lost" a few presentations that were attached to the "proposal" assessment
item rather than the "presentation" assessment item.  My mistake.

This is an interesting paper.
Definitely solve the non-sales version first.  You should aim to solve the LP relaxation by column
generation, which means you will need to be able to solve the sub-problems.  Don't worry if you
don't get Branching implemented as it can be very difficult to get right.


Presentation feedback,

You will need to focus on solving the RMP for the full MBRP, using CG. A full Branching implementation
is probably too ambitious at this stage.

Overleaf report link: https://www.overleaf.com/project/5f96aa69f5223f0001d7dd3f
"""

import copy
import random
from typing import Any
from typing import List, Dict, Tuple

import numpy as np
from gurobipy import *

from nic_helper import get_problem_data, MbrpData

ROUTE_NONE = "No route yet specified"
ROUTE_INVALID = "Invalid route"
ROUTE_FAILED = "Failed route"

ROUTE_NAN = "-"
ROUTE_NO_PRODUCE = "No produce"

MODEL_FAILED = "Model failed"

EPSILON = 1e-6

random.seed(4202)


class Route:

    def __init__(self, visits, prod_start=None, prod_end=None, market=None, total_revenue=None):
        self.visits: List[int] = visits
        self.prod_start: List[float] = prod_start
        self.prod_end: List[float] = prod_end
        self.market: List[int] = market
        self.total_revenue: float = total_revenue

        self.n_gamma = 0

    def __repr__(self):
        return f"Route({self.total_revenue}, {tuple(self.visits)})"

    def update_n_gamma(self, val):
        self.n_gamma += val


def main():
    """Main execution for the model"""

    # ----------------- Phase 0 Preparation Works -----------------
    nectar_sources = "25"  # {'25', '50', '100'}
    instance = "01"  # {01, 02, ..., 10}
    terminus = (100.0, 100.0)  # (6.0, 57.0)
    data = get_problem_data(nectar_sources, instance, terminus, False, 99999)
    print("~" * 60, "\nData for model retrieved\n", "~" * 60)

    # ----------------- Phase 1 Route initialisation -----------------
    env_capacity = copy.copy(data.param_env_capacity)
    routes_todo = [(k, m) for k in data.set_k for m in data.set_m[k]]
    route_set = {}

    run_route_init = True
    if run_route_init:
        # Greedy route finding
        while routes_todo:
            todo = routes_todo.pop(0)
            solution = solve_pricing_sub_problem(data, *todo, None, None, 0)

            # Update route set
            nk_rest = len(routes_todo)
            n_gamma = min(*(data.param_env_capacity[i] for i in solution.visits[1:-1]), nk_rest)

            route_set[todo] = (solution, n_gamma)

            # Update copy of environment capacity
            for i in solution.visits[1:-1]:
                env_capacity[i] -= n_gamma

            # Set the updated environment capacity
            data.param_env_capacity = copy.copy(env_capacity)

            print(f"Calculated route for group {todo}")

        for k, v in route_set.items():
            print(f"{k}: (Route(visits={v[0].visits}), {v[1]}),")

    else:
        mock = Route(visits=[0, 18, 20, 7, 13, 17, 15, 9, 10, 19, 26],
                     prod_start=['-', 44.0, 86.0, 163.0, 196.0, 225.0, 242.0, 285.0, 310.0, 350.0, '-'],
                     prod_end=['-', 75.0, 102.0, 190.0, 218.0, 236.0, 273.0, 306.0, 339.0, 365.0, '-'],
                     market=[0, 18, 20, 7, 13, 17, 15, 9, 10, 19, 26],
                     total_revenue=160629.0)
        route_set = {(k, m): (copy.copy(mock), 0.0) for k in data.set_k for m in data.set_m[k]}

    print("~" * 60, "\nGreedy route initialisation complete\n", "~" * 60)

    # ----------------- Phase 3 - MBRP using CG -----------------

    # Reset data
    data = get_problem_data(nectar_sources, instance, terminus, False, 999)

    # Add in route straight to terminus
    route_to_terminus = {k: Route(visits=(k, data.terminus_idx),
                                  prod_start=[ROUTE_NAN, ROUTE_NAN],
                                  prod_end=[ROUTE_NAN, ROUTE_NAN],
                                  market=[0, 26],
                                  total_revenue=-data.param_l)
                         for k in data.set_k}

    # Parse the data from the initial route generation
    # Maps the route visits to the route object they came from
    route_map = {tuple(route.visits): route for route, _ in route_set.values()}
    for k in data.set_k:
        route_map[tuple(route_to_terminus[k].visits)] = route_to_terminus[k]

    # Valid routes from origin k for beekeeper group m
    valid_routes = {(k, m): [route_set[k, m]] + [(route_to_terminus[k], 100)]
                    for (k, m), route in route_set.items()}

    # Instantiate all possible routes
    all_routes: Dict[int: List[Tuple[Route, List[int]]]] = {i: [] for i in data.set_total_idx}

    # Build out the RMP
    rmp = Model("Restricted Master Problem")

    # All possible valid routes
    all_valid_routes = {
        (k, tuple(route.visits)): (route.total_revenue, capacity)
        for k in data.set_k
        for m in data.set_m[k]
        for route, capacity in valid_routes[k, m]
    }

    all_valid_beekeeper_routes = {
        (k, m, tuple(route.visits)): (route.total_revenue, capacity)
        for k in data.set_k
        for m in data.set_m[k]
        for route, capacity in valid_routes[k, m]
    }

    # Main variable for RMP
    z = {r: rmp.addVar() for r in all_valid_routes}

    # RMP Objective
    rmp.setObjective(quicksum(all_valid_routes[r][0] * z[r] for r in z), GRB.MAXIMIZE)

    # Constraints
    constraint_groups_from_origin = {
        k: rmp.addConstr(quicksum(z[r] for r in all_valid_routes if r[0] == k) == len(data.set_m[k]))
        for k in data.set_k
    }

    def indicator(route: Route, j: int) -> int:
        """Return 1 if the route stops at j for a period of time, 0 otherwise"""
        if j not in route.visits:
            return 0
        else:
            idx = route.visits.index(j)
            t_j = route.prod_end[idx] - route.prod_start[idx]
            return 1 if t_j > 0 else 0

    x_gamma = {
        (k, i, j, r): 1 if (i, j) in get_trip_arcs(r) else 0
        for k in data.set_k
        for i in data.set_total_idx
        for j in data.set_total_idx
        for r_k, r in all_valid_routes
        if k == r_k
    }

    #######################################################################
    # Testing code
    #######################################################################
    testing = False
    if testing:
        out = set()
        for j in data.set_nectar_idx:
            for k in data.set_k:
                for r_k, r in all_valid_routes:
                    if r_k == k:
                        # This should only ever equal 1
                        a = (sum(x_gamma[k, i, j, r]
                                 for i in [k] + data.set_nectar_idx))
                        out.add(a)

                        print(k, j,
                              sum(x_gamma[k, i, j, r] * indicator(route_map[r], j)
                                  for m in data.set_m[k]
                                  for i in [k] + data.set_nectar_idx),
                              math.floor(data.param_env_capacity[j] / data.n_colonies))
        print("Sum of x lambda set: ", out)
    #######################################################################

    constraint_env_capacity = {
        j: rmp.addConstr(quicksum(z[r_k, r] * sum(x_gamma[k, i, j, r] *
                                                  indicator(route_map[r], j)
                                                  for i in [k] + data.set_nectar_idx)
                                  for k in data.set_k
                                  for (r_k, r) in all_valid_routes
                                  if r_k == k)
                         <= math.floor(data.param_env_capacity[j] / data.n_colonies))
        for j in data.set_nectar_idx
    }

    rmp.setParam("MIPGap", 0)
    rmp.setParam('OutputFlag', 1)

    def generate_columns(dual_0, dual_1):
        # All routes, for nectar source i, all_routes[i] gives a list of tuples, where the first element is a
        # possible route and the second element is the tabu list associated with that route
        nodes_remaining = list(data.set_total_idx)
        global_lb = 9999

        total_new = 0

        # Route initialization (Υ[l]) by accessing the terminus; thereafter, attached a tabu list that contains
        # all nectar sources that have been attempted to append to the label
        # idx = nodes_remaining.pop(nodes_remaining.index(data.terminus_idx))
        all_routes[data.terminus_idx].append(
            (Route(visits=[data.terminus_idx],
                   prod_start=[365],
                   prod_end=[365],
                   market=[26],
                   total_revenue=-data.param_l), [])
        )

        # Loop the following steps when Λ is not null
        while nodes_remaining:

            # Step 4a.
            # i is the node we're focusing on this loop, j are all possible extensions from i. Always do terminus first
            if data.terminus_idx in nodes_remaining:
                i = nodes_remaining.pop(nodes_remaining.index(data.terminus_idx))
            else:
                i = nodes_remaining.pop(nodes_remaining.index(np.random.choice(nodes_remaining)))

            # Step 4b.
            set_j = [j for j in data.set_k + data.set_nectar_idx if data.adjacency_matrix[j][i]]
            for j in set_j:

                new_labels = []

                # Step 4b(i).
                for route_i, tabu in all_routes[i]:

                    # 4b(ii). If j in tabu list, ignore
                    if j in tabu or i == j or j in route_i.visits:
                        continue
                    else:
                        tabu.append(j)
                        new_route_j = extend_route(route_i, data, j, dual_1)
                        if new_route_j == MODEL_FAILED:
                            continue

                        # 4b(iii). If not already in all routes, add to new labels set
                        if new_route_j not in all_routes[j]:
                            new_labels.append((new_route_j, []))

                # TODO Dominance round

                # Step 4c. Remove non-dominated labels, then add NL to all routes,
                # add j to nodes remaining if new labels is not None
                if new_labels:
                    for route, t in new_labels:
                        # Exclude labels from origin if they have negative reduced costs
                        # if j in data.set_k:
                        #     if label.total_revenue - dual_0[j] <= 0:
                        #         continue

                        all_routes[j].append((route, t))

                        # If label is an origin region, add to main problem data
                        if j in data.set_k:

                            k = j  # For consistency

                            # Find route capacity, if goes straight from origin to terminus, capacity is infinite
                            route_capacity = min(data.param_env_capacity[i] for i in route.visits[1:-1]) + 1 \
                                if len(route.visits) > 3 else 100

                            # Add route into our data structures
                            all_valid_routes[route.visits[0], tuple(route.visits)] = route.total_revenue, route_capacity
                            route_map[tuple(route.visits)] = route

                            # Incorporate new variable into the model
                            z[k, tuple(route.visits)] = new_var = rmp.addVar(obj=route.total_revenue)
                            rmp.chgCoeff(constraint_groups_from_origin[k], new_var, 1)
                            for jj in route.visits[1:-1]:
                                rmp.chgCoeff(constr=constraint_env_capacity[jj],
                                             var=new_var,
                                             newvalue=sum(int((ii, jj) in get_trip_arcs(route.visits)) *
                                                          indicator(route, jj)
                                                          for ii in [k] + data.set_nectar_idx))

                            total_new += 1

            print(f"Generated new routes from {i}")

        return total_new

    while True:
        rmp.optimize()
        try:
            print('Obj val:', rmp.objVal)
        except AttributeError:
            print("RMP failed")

        # Solve all the sub-problems
        pi_0 = [i.pi for i in constraint_groups_from_origin.values()]
        pi_1 = [i.pi for i in constraint_env_capacity.values()]

        # Generate new columns
        added = generate_columns(pi_0, pi_1)
        print(f"\nAdded {added} new variables to the RMP\n")

        if added == 0:
            break

    print("Debug line")


def get_trip_arcs(trip):
    return [(trip[i], trip[i + 1]) for i in range(len(trip) - 1)]


def extend_route(route: Route, data: MbrpData, new_location: int, dual_1: List[float]):
    """

    Args:
        route:
        data:
        new_location:
        dual_1:

    Returns:

    """

    # Step 2. Obtain the prices according to the visited locations in γi

    # Step 3. . Obtain the influenced nectar sources, {vn}, according to Theorem 4
    influenced = [
        j for idx, j in enumerate(route.visits)
        if data.param_flower_end[new_location] + data.shortest_path[new_location, route.visits[0]] +
           sum([max(data.shortest_path[route.visits[m], route.visits[m + 1]],
                    data.param_flower_start[route.visits[m + 1]] - data.param_flower_end[route.visits[m]])
                for m in range(idx)]) > route.prod_start[idx]
    ]

    #################################################################################
    # Testing influence
    #################################################################################
    test = False
    if test:
        for idx, i in enumerate(route.visits):
            print(i == data.terminus_idx)
            print(data.param_flower_end[new_location] + data.shortest_path[new_location, route.visits[0]])
            print(sum([max(data.shortest_path[route.visits[m], route.visits[m + 1]],
                           data.param_flower_start[route.visits[m + 1]] -
                           data.param_flower_end[route.visits[m]]) for m in range(idx)]))
            for m in range(idx):
                print(m, max(data.shortest_path[route.visits[m], route.visits[m + 1]],
                             data.param_flower_start[route.visits[m + 1]] -
                             data.param_flower_end[route.visits[m]]))
            print(route.prod_start[idx], "\n\n")
    #################################################################################

    if not influenced:

        prod_start = [data.param_flower_start[new_location]] + route.prod_start
        prod_end = [data.param_flower_end[new_location]] + route.prod_end

        revenue = route.total_revenue + \
                  data.non_sales_param_yield[new_location - data.n_origins] * \
                  data.non_sales_param_price[new_location - data.n_origins] * \
                  (prod_start[0] - prod_end[0]) - \
                  data.param_cost[new_location][route.visits[0]]

        new_route = Route(visits=[new_location] + route.visits,
                          prod_start=prod_start,
                          prod_end=prod_end,
                          market=[data.terminus_idx] + route.market,
                          total_revenue=revenue)

    else:

        set_i = [new_location] + route.visits
        set_i = list(filter(lambda x: x not in data.set_k + [data.terminus_idx], set_i))

        # Run and solve model for any nectar source existing
        model = Model("Influenced sources")

        # t[i] -> float, production start time of beekeeper group m from origin region k at nectar source location i
        t_prod_start = {i: model.addVar() for i in set_i}

        # t[i] -> float, production time end of beekeeper group m from origin region k at nectar source location i
        t_prod_end = {i: model.addVar() for i in set_i}

        # Linearized sign variable
        sgn = {i: model.addVar(vtype=GRB.BINARY) for i in set_i}

        # Objective
        model.setObjective(quicksum(data.non_sales_param_yield[i - data.n_origins] *
                                    data.non_sales_param_price[i - data.n_origins] *
                                    (t_prod_end[i] - t_prod_start[i])
                                    for i in set_i) -
                           quicksum(dual_1[i - data.n_origins] * sgn[i] for i in set_i),
                           GRB.MAXIMIZE)

        # Constraints
        constraint_production_bounds = {
            i: model.addConstr(t_prod_end[i] - t_prod_start[i] <=
                               (data.param_flower_end[i] - data.param_flower_start[i]) * sgn[i])
            for i in set_i
        }

        oop_constraint_0 = {i: model.addConstr(data.param_flower_start[i] <= t_prod_start[i]) for i in set_i}

        oop_constraint_1 = {i: model.addConstr(t_prod_start[i] <= t_prod_end[i]) for i in set_i}

        oop_constraint_2 = {i: model.addConstr(t_prod_end[i] <= data.param_flower_end[i]) for i in set_i}

        constraint_timing = {
            i: model.addConstr(t_prod_end[i] + math.ceil(data.param_distance[i][j] / data.transport_speed)
                               <= t_prod_start[j])
            for i, j in get_trip_arcs(set_i)
        }

        # Exclude terminus node here
        constraint_timing_1 = model.addConstr(t_prod_end[set_i[-1]] <= route.prod_end[-1])

        # Equation 23, linearize sign variable
        constraint_linearize_sgn = {
            i: model.addConstr(data.param_l * sgn[i] >= t_prod_end[i] - t_prod_start[i])
            for i in set_i
        }

        # Optimise
        model.setParam('OutputFlag', 0)
        model.optimize()

        try:
            model.objVal
        except AttributeError:
            # TODO this is very wrong
            # print("Model failed")
            return MODEL_FAILED

        visits = market = [new_location] + route.visits
        prod_start = [t_prod_start[i].x for i in set_i] + \
                     [t_prod_start[set_i[-1]].x + data.shortest_path[set_i[-1], data.terminus_idx]]
        prod_end = [t_prod_end[i].x for i in set_i] + \
                   [t_prod_end[set_i[-1]].x + data.shortest_path[set_i[-1], data.terminus_idx]]

        new_route = Route(visits=visits,
                          prod_start=prod_start,
                          prod_end=prod_end,
                          market=market,
                          total_revenue=-data.param_l)

    return new_route


def solve_pricing_sub_problem(data: MbrpData, k: int, m: int, pi_0: Any, pi_1: Any, verbose: int = 0) -> Route:
    """
    Solve the pricing sub-problem for beekeeper group m from origin k

    Args:
        data:
        k:
        m:
        pi_0:
        pi_1:
        verbose

    Returns:

    """

    model = Model("Pricing sub-problem")

    # ----------------- Variables -----------------
    # x[i, j] -> {0, 1}, equals 1 if beekeeper departs i to j
    # Note that this includes origin regions -> [origin_locations, nectar locations, terminal locations]
    x = {
        (i, j): model.addVar(vtype=GRB.BINARY)
        for i in [k] + data.set_nectar_idx
        for j in data.set_nectar_terminus_idx
        if i != j
    }

    # z[i, j] -> {0, 1}
    z = {
        (i, j): model.addVar(vtype=GRB.BINARY)
        for i in data.set_nectar_idx
        for j in data.set_u[i]
        if i != j
    }

    # W[i, j] -> 0.0+
    # Note that this includes origin regions -> [origin_locations, nectar locations, terminal locations]
    w = {
        (i, j): model.addVar(lb=0.0)
        for i in [k] + data.set_nectar_idx
        for j in data.set_u[i]
        if i != j
    }

    # t[i] -> float, arrival time of beekeeper group m from origin region k at nectar source location i
    t_arrival = {i: model.addVar(lb=0) for i in data.set_nectar_terminus_idx}

    # t[i] -> float, production start time of beekeeper group m from origin region k at nectar source location i
    t_prod_start = {i: model.addVar() for i in data.set_nectar_idx}

    # t[i] -> float, production time end of beekeeper group m from origin region k at nectar source location i
    t_prod_end = {i: model.addVar() for i in data.set_nectar_idx}

    # t[i] -> float, departure time of beekeeper group m from origin region k at nectar source location i
    t_departure = {i: model.addVar() for i in [k] + data.set_nectar_idx}

    # t[i] -> 0.0+
    # t[i] = time_end[i] - time_start[i]
    # t = {i: model.addVar(lb=0.0) for i in data.set_nectar_idx}
    t = {i: t_prod_end[i] - t_prod_start[i] for i in data.set_nectar_idx}

    # Linearized sign variable
    sgn = {i: model.addVar(vtype=GRB.BINARY) for i in data.set_nectar_idx}

    # ----------------- Objective -----------------

    # Solve the non-sales MBRP
    if pi_0 is None and pi_1 is None:
        # 1. the trade-off between the total income from production and the total cost of migration
        model.setObjective(
            data.n_colonies * quicksum(
                data.non_sales_param_price[i - data.n_origins] *
                data.non_sales_param_yield[i - data.n_origins] *
                (t_prod_end[i] - t_prod_start[i])
                for i in data.set_nectar_idx
            ) - quicksum(
                data.param_cost[i][j] * x[i, j]
                for i in [k] + data.set_nectar_idx
                for j in data.set_nectar_terminus_idx
                if i != j
            ),
            GRB.MAXIMIZE
        )

    else:

        # 1. the trade-off between the total income from production and the total cost of migration
        model.setObjective(
            data.n_colonies *
            quicksum(data.param_yield[i][g] * quicksum(data.param_price[j][g] * w[i, j]
                                                       for j in data.set_u[i]
                                                       if i != j)
                     for i in data.set_nectar_idx
                     for g in data.set_g) -
            quicksum(data.param_cost[i][j] * x[i, j]
                     for i in [k] + data.set_nectar_idx
                     for j in data.set_nectar_terminus_idx
                     if i != j) -
            quicksum(pi_1[i - data.n_origins] * sgn[i] for i in data.set_nectar_idx) - pi_0[k],
            GRB.MAXIMIZE
        )

    # ----------------- Constraints -----------------

    # Equation 31
    constraint_31_1 = model.addConstr(quicksum(x[k, j] for j in data.set_nectar_idx) == 1)
    constraint_31_2 = model.addConstr(quicksum(x[i, data.terminus_idx] for i in data.set_nectar_idx) == 1)

    # Equation 32
    constraint_32_1 = {
        j: model.addConstr(
            quicksum(x[i, j] for i in [k] + data.set_nectar_idx if i != j) ==
            quicksum(x[j, i] for i in data.set_nectar_terminus_idx if i != j))
        for j in data.set_nectar_idx
    }

    constraint_32_2 = {
        j: model.addConstr(quicksum(x[j, i] for i in data.set_nectar_terminus_idx if i != j) <= 1)
        for j in data.set_nectar_idx
    }

    # Equation 33
    # Forces z[i, j] if i and j is not visited via x
    constraint_33 = {
        (i, j):
            model.addConstr(z[i, j] <= quicksum(x[ii, j] for ii in [k] + data.set_nectar_idx if ii != j))
        for i in data.set_nectar_idx
        for j in data.set_u[i]
        if i != j
    }

    # Equation 34
    # Requires the balance between production and sales
    constraint_34 = {
        i: model.addConstr(quicksum(z[i, j] for j in data.set_u[i] if i != j) <= 1)
        for i in data.set_nectar_idx
    }

    # Equation 35
    constraint_35 = {
        i: model.addConstr(t_prod_end[i] - t_prod_start[i] <=
                           (data.param_flower_end[i] - data.param_flower_start[i]) *
                           quicksum(x[i, j] for j in data.set_nectar_terminus_idx if i != j))
        for i in data.set_nectar_idx
    }

    # Equation 36
    constraint_36 = {
        (i, j):
            model.addConstr(t_prod_end[i] - t_prod_end[j] <=
                            data.param_l * (1 - z[i, j]))
        for i in data.set_nectar_idx
        for j in data.set_u[i]
        if i != j
    }

    # Equation 37
    constraint_37 = {
        (i, j): model.addConstr(t_departure[i] + math.ceil(data.param_distance[i][j] / data.transport_speed) -
                                data.param_l * (1 - x[i, j]) <= t_arrival[j])
        for i in [k] + data.set_nectar_idx
        for j in data.set_nectar_terminus_idx
        if i != j
    }

    # Equation 38
    constraint_38_1 = {i: model.addConstr(data.param_flower_start[i] <= t_prod_start[i]) for i in data.set_nectar_idx}

    constraint_38_2 = {i: model.addConstr(t_prod_start[i] <= t_prod_end[i]) for i in data.set_nectar_idx}

    constraint_38_3 = {i: model.addConstr(t_prod_end[i] <= data.param_flower_end[i]) for i in data.set_nectar_idx}

    # Equation 39
    constraint_39 = {
        (i, j):
            model.addConstr(w[i, j] >= t[i] +
                            (data.param_flower_end[i] - data.param_flower_start[i]) * z[i, j] -
                            (data.param_flower_end[i] - data.param_flower_start[i]))
        for i in data.set_nectar_idx
        for j in data.set_u[i]
        if i != j
    }

    # Equation 40
    constraint_40 = {
        (i, j): model.addConstr(w[i, j] <= t[i])
        for i in data.set_nectar_idx
        for j in data.set_u[i]
        if i != j
    }

    # Equation 41
    constraint_41 = {
        (i, j): model.addConstr(w[i, j] <= (data.param_flower_end[i] - data.param_flower_start[i]) * z[i, j])
        for i in data.set_nectar_idx
        for j in data.set_u[i]
        if i != j
    }

    # 11 - 13. All related to the timing rules illustrated in figure 2 of the paper
    operations_order_0 = {i: model.addConstr(t_arrival[i] <= t_prod_start[i]) for i in data.set_nectar_idx}
    operations_order_1 = {i: model.addConstr(t_prod_start[i] <= t_prod_end[i]) for i in data.set_nectar_idx}
    operations_order_2 = {i: model.addConstr(t_prod_end[i] <= t_departure[i]) for i in data.set_nectar_idx}

    # Equation 23, linearize sign variable
    constraint_linearize_sgn = {
        i: model.addConstr(data.param_l * sgn[i] >= t_prod_end[i] - t_prod_start[i])
        for i in data.set_nectar_idx
    }

    # ----------------- Run model -----------------
    model.setParam('OutputFlag', 0)
    model.optimize()

    # ----------------- Interpret model -----------------
    # Interpret results in line with how the original paper does
    beekeeper_movements = {}

    # Sales
    for i in data.set_nectar_idx:
        for j in data.set_u[i]:

            if i != j and z[(i, j)].x > 0.9:
                # Beekeeper group m from origin k moves from location i to j
                # Record arrivals, departures, and production times for location i

                if verbose != 0:
                    print(f"Group sells produce from {i} at {j}")

    if verbose != 0:
        print("Values for w")
        for kk, v in w.items():
            print(kk, v.x)

    # For beekeeper group m from origin k
    for i in [k] + data.set_nectar_idx:
        for j in data.set_nectar_terminus_idx:

            if i != j and x[(i, j)].x > 0.9:
                # Beekeeper group m from origin k moves from location i to j
                # Record arrivals, departures, and production times for location i

                if verbose != 0:
                    print(f"Group moves from {i} to {j}")

                if i in data.set_origin_idx:
                    beekeeper_movements[i] = (
                        0.0,  # arrived
                        ROUTE_NAN,  # prod start
                        ROUTE_NAN,  # prod end
                        round(t_departure[i].x, 2),  # left
                    )

                elif j == data.terminus_idx:

                    beekeeper_movements[i] = (
                        round(t_arrival[i].x, 2),
                        round(t_prod_start[i].x, 2),
                        round(t_prod_end[i].x, 2),
                        round(t_departure[i].x, 2),
                    )

                    beekeeper_movements[j] = (
                        round(t_arrival[j].x, 2),  # arrived
                        ROUTE_NAN,  # prod start
                        ROUTE_NAN,  # prod end
                        ROUTE_NAN,  # left
                    )

                else:
                    beekeeper_movements[i] = (
                        round(t_arrival[i].x, 2),
                        round(t_prod_start[i].x, 2),
                        round(t_prod_end[i].x, 2),
                        round(t_departure[i].x, 2),
                    )

    beekeeper_movements = {k: v for k, v in sorted(beekeeper_movements.items(), key=lambda item: item[1][0])}
    if verbose != 0:
        print("\n\tLocation\tArrived, Start, End, Depart")
        print("=" * 70)
        for kk, v in beekeeper_movements.items():
            print("\t{}\t\t\t{}, {}, {}, {}".format(kk, *v))

    # Format route for output
    sequential_visits = list(beekeeper_movements.keys())
    prod_start = [i[1] for i in beekeeper_movements.values()]
    prod_end = [i[2] for i in beekeeper_movements.values()]
    revenue = model.ObjVal

    if pi_0 is None and pi_1 is None:
        # If solving the non-sales
        market = sequential_visits
    else:
        sales = {
            i: j
            for i in data.set_nectar_idx
            for j in data.set_u[i]
            if i != j and z[i, j].x > 0.9
        }

        market = [ROUTE_NAN] + [sales[i] if i in sales else ROUTE_NO_PRODUCE
                                for i in sequential_visits[1:-1]] + [ROUTE_NAN]

    return Route(visits=sequential_visits,
                 prod_start=prod_start,
                 prod_end=prod_end,
                 market=market,
                 total_revenue=revenue)


if __name__ == '__main__':
    main()
