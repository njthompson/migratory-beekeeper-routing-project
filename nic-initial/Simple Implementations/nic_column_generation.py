from nic_helper import MbrpData
from typing import List, Dict, Tuple
import numpy as np






#####################################################################################
# Redundant code
#####################################################################################


def generate_all_routes(data: MbrpData):
    all_routes = {}
    sources_remaining = list(data.set_nectar_terminus_idx)

    while sources_remaining:
        i = sources_remaining.pop(0)
        for j in data.set_nectar_idx:
            if data.adjacency_matrix[i - data.n_origins][j - data.n_origins]:

                for route, tabu in all_routes.items():
                    if j in tabu:
                        continue
                    else:
                        pass

    return all_routes


def generate_all_routes_2(data: MbrpData):
    all_routes = set()

    def generate_trips(trip):
        if trip[-1] == data.terminus_idx:
            all_routes.add(trip)
            return

        for j, adj in zip(data.set_nectar_terminus_idx, data.adjacency_matrix[trip[-1] - data.n_origins] + [1]):
            if j not in trip and trip[-1] != j and adj:
                generate_trips(trip + (j,))

    generate_trips(trip=(0,))
    return all_routes

