from gurobipy import *

from nic_helper import (
    MbrpData
)


def solve_non_sales_mbrp(data: MbrpData):
    """Main execution for the model"""

    # ----------------- Model -----------------
    model = Model("Non-sales MBRP")

    # ----------------- Variables -----------------

    # X[k, m, i, j] -> {0, 1}, equals 1 if beekeeper group m from origin k depart i to j
    # Note that this includes origin regions -> [origin_locations, nectar locations, terminal locations]
    x = {
        (k, m, i, j): model.addVar(vtype=GRB.BINARY)
        for k in data.set_k
        for m in data.set_m[k]
        for i in [k] + data.set_nectar_idx
        for j in data.set_nectar_terminus_idx
        if i != j
    }

    # t[k, m, i] -> float, arrival time of beekeeper group m from origin region k at nectar source location i
    t_arrival = {(k, m, i): model.addVar(lb=0) for k in data.set_k for m in data.set_m[k] for i in
                 data.set_nectar_terminus_idx}

    # t[k, m, i] -> float, production start time of beekeeper group m from origin region k at nectar source location i
    t_prod_start = {(k, m, i): model.addVar() for k in data.set_k for m in data.set_m[k] for i in data.set_nectar_idx}

    # t[k, m, i] -> float, production time end of beekeeper group m from origin region k at nectar source location i
    t_prod_end = {(k, m, i): model.addVar() for k in data.set_k for m in data.set_m[k] for i in data.set_nectar_idx}

    # t[k, m, i] -> float, departure time of beekeeper group m from origin region k at nectar source location i
    t_departure = {
        (k, m, i): model.addVar()
        for k in data.set_k
        for m in data.set_m[k]
        for i in [k] + data.set_nectar_idx
    }

    # ----------------- Objective -----------------

    # # Debugging
    # for k in set_k:
    #     for m in set_m[k]:
    #         for i in set_nectar_idx:
    #             for g in set_g:
    #                 try:
    #                     print(param_yield[i][g])
    #                     print(t_prod_end[(k, m, i)])
    #                     print(t_prod_start[(k, m, i)])
    #                     print([param_price[j][g] for j in set_u[i]])
    #                     print([z[(k, m, i, j)] for j in set_u[i]])
    #                 except IndexError:
    #                     print("Error on", k, m, i, g)
    #                     break

    # 1. the trade-off between the total income from production and the total cost of migration
    model.setObjective(
        data.param_bee_avg * quicksum(
            data.non_sales_param_price[i - data.n_origins] *
            data.non_sales_param_yield[i - data.n_origins] *
            (t_prod_end[(k, m, i)] - t_prod_start[(k, m, i)])
            for k in data.set_k
            for m in data.set_m[k]
            for i in data.set_nectar_idx
        ) - quicksum(
            data.param_cost[i][j] * x[(k, m, i, j)]
            for k in data.set_k
            for m in data.set_m[k]
            for i in [k] + data.set_nectar_idx
            for j in data.set_nectar_terminus_idx
            if i != j
        ),
        GRB.MAXIMIZE
    )

    # ----------------- Constraints -----------------

    # 2 & 3. each beekeeper group must depart from his or her origin region and arrive at the virtual terminus. Assumes
    # there only exists one terminus. i in this equation references the x value of the origin k + n_nectar_locations
    # Since set_k as origins indexed from 0 to n_origins, x_i is similarly indexed by k
    depart_from_origin = {
        (k, m):
            model.addConstr(quicksum(x[(k, m, k, j)] for j in data.set_nectar_idx) == 1)
        for k in data.set_k
        for m in data.set_m[k]
    }

    arrive_at_terminus = {
        (k, m):
            model.addConstr(quicksum(x[(k, m, i, data.terminus_idx)] for i in data.set_nectar_idx) == 1)
        for k in data.set_k
        for m in data.set_m[k]
    }

    # 4. flow conservation at each nectar source location
    flow_conservation = {
        (k, m, j):
            model.addConstr(quicksum(x[(k, m, i, j)] for i in [k] + data.set_nectar_idx if i != j) ==
                            quicksum(x[(k, m, j, i)] for i in data.set_nectar_terminus_idx if i != j))
        for k in data.set_k
        for m in data.set_m[k]
        for j in data.set_nectar_idx
    }

    flow_conservation_2 = {
        (k, m, j):
            model.addConstr(quicksum(x[(k, m, j, i)] for i in data.set_nectar_terminus_idx if i != j) <= 1)
        for k in data.set_k
        for m in data.set_m[k]
        for j in data.set_nectar_idx
    }

    # 7. limit the environmental carrying capacity of each nectar source location, where the sign function sgn()
    # guarantees that a nectar source will not be occupied by any beekeeper group that only passes through there
    # without production (i.e., the production time of such a group is zero)

    funky_sgn_way = False
    if funky_sgn_way:

        sgn = {
            (k, m, i):
                model.addVar(vtype=GRB.BINARY)
            for k in data.set_k
            for m in data.set_m[k]
            for i in data.set_nectar_idx
        }

        min_time = 0.1
        eps, lb, ub = 1e-6, 0.5, 365
        sgn_constraint_1 = {
            (k, m, i): model.addConstr(
                t_prod_end[(k, m, i)] - t_prod_start[(k, m, i)] <= min_time - eps + (ub - min_time + eps) * sgn[k, m, i])
            for k in data.set_k
            for m in data.set_m[k]
            for i in data.set_nectar_idx
        }

        sgn_constraint_2 = {
            (k, m, i): model.addConstr(
                t_prod_end[(k, m, i)] - t_prod_start[(k, m, i)] <= min_time - eps + (ub - min_time + eps) * sgn[
                    k, m, i])
            for k in data.set_k
            for m in data.set_m[k]
            for i in data.set_nectar_idx
        }

        env_capacity_limit = {
            j: model.addConstr(
                data.param_bee_avg * quicksum(sgn[(k, m, j)] *
                                              (t_prod_end[(k, m, j)] -
                                               t_prod_start[(k, m, j)])
                                              for k in data.set_k
                                              for m in data.set_m[k])
                <= data.param_env_capacity[j])
            for j in data.set_nectar_idx
        }

    else:
        def sgn(xx):
            # https://support.gurobi.com/hc/en-us/community/posts/360071561532-implement-conditional-constraint
            return xx  # TODO implement this function

        env_capacity_limit = {
            j: model.addConstr(data.param_bee_avg * quicksum(
                sgn(t_prod_end[(k, m, j)] - t_prod_start[(k, m, j)]) for k in data.set_k for m in data.set_m[k]
            ) <= data.param_env_capacity[j]) for j in data.set_nectar_idx
        }

    # 9. Denotes that t_prod_start[i] and t_prod_end[i] should be equal if nectar source i is not visited
    # (i.e., i has no yield)
    source_not_visited = {
        (k, m, i): model.addConstr(t_prod_end[(k, m, i)] - t_prod_start[(k, m, i)] <=
                                   data.param_l * quicksum(
            x[(k, m, i, j)] for j in data.set_nectar_terminus_idx if i != j))
        for k in data.set_k
        for m in data.set_m[k]
        for i in data.set_nectar_idx
    }

    # 10. Express the time relationship among sequentially accessed locations, where the ceiling function returns the
    # minimal integer number that is greater than or equal to the input parameter
    sequential_time = {
        (k, m, i, j):
            model.addConstr(t_departure[(k, m, i)] +
                            math.ceil(data.param_distance[i][j] / data.transport_speed)
                            - data.param_l *
                            (1 - x[(k, m, i, j)])
                            <= t_arrival[(k, m, j)])
        for k in data.set_k
        for m in data.set_m[k]
        for i in [k] + data.set_nectar_idx
        for j in data.set_nectar_terminus_idx
        if i != j
    }

    # 11 - 13. All related to the timing rules illustrated in figure 2 of the paper
    # TODO is there a more efficient way to combine these inequalities?
    operations_order_0 = {
        (k, m, i):
            model.addConstr(t_arrival[(k, m, i)] <= t_prod_start[(k, m, i)])
        for k in data.set_k
        for m in data.set_m[k]
        for i in data.set_nectar_idx
    }

    operations_order_1 = {
        (k, m, i):
            model.addConstr(t_prod_start[(k, m, i)] <= t_prod_end[(k, m, i)])
        for k in data.set_k
        for m in data.set_m[k]
        for i in data.set_nectar_idx
    }

    operations_order_2 = {
        (k, m, i):
            model.addConstr(t_prod_end[(k, m, i)] <= t_departure[(k, m, i)])
        for k in data.set_k
        for m in data.set_m[k]
        for i in data.set_nectar_idx
    }

    start_after_flowering_open = {
        (k, m, i):
            model.addConstr(data.param_flower_start[i] <= t_prod_start[(k, m, i)])
        for k in data.set_k
        for m in data.set_m[k]
        for i in data.set_nectar_idx
    }

    end_before_flowering_close = {
        (k, m, i):
            model.addConstr(t_prod_end[(k, m, i)] <= data.param_flower_end[i])
        for k in data.set_k
        for m in data.set_m[k]
        for i in data.set_nectar_idx
    }

    # 14 - 16. definition of timing decision variables, routing decision variables, and remote
    # sales decision variables. These are handled in variable definitions

    # ----------------- Run model -----------------
    model.optimize()

    # ----------------- Results interpretation -----------------

    # X[k, m, i, j] -> {0, 1}, equals 1 if beekeeper group m from origin k depart i to j
    # Z[k, m, i, j] -> {0, 1}, indicates whether honey products produced at location i will be sold at location j for
    #   beekeeper group m from origin region k
    # t_arrival[k, m, i] -> float, arrival time of group m from origin region k at nectar source location i
    # t_prod_start[k, m, i] -> float, production start time of group m from origin region k at nectar source location i
    # t_prod_end[k, m, i] -> float, production time end of group m from origin region k at nectar source location i
    # t_departure[k, m, i] -> float, departure time of group m from origin region k at nectar source location i

    # Interpret results in line with how the original paper does
    beekeeper_movements = {(k, m): {} for k in data.set_k for m in data.set_m[k]}
    for k in data.set_k:
        for m in data.set_m[k]:

            # For beekeeper group m from origin k
            for i in [k] + data.set_nectar_idx:
                for j in data.set_nectar_terminus_idx:

                    if i != j and x[(k, m, i, j)].x > 0.9:
                        # Beekeeper group m from origin k moves from location i to j
                        # Record arrivals, departures, and production times for location i

                        if i in data.set_origin_idx:
                            beekeeper_movements[(k, m)][i] = (
                                0.0,  # arrived
                                "-",  # prod start
                                "-",  # prod end
                                round(t_departure[(k, m, i)].x, 2),  # left
                            )

                        elif j == data.terminus_idx:

                            beekeeper_movements[(k, m)][i] = (
                                round(t_arrival[(k, m, i)].x, 2),
                                round(t_prod_start[(k, m, i)].x, 2),
                                round(t_prod_end[(k, m, i)].x, 2),
                                round(t_departure[(k, m, i)].x, 2),
                            )

                            beekeeper_movements[(k, m)][j] = (
                                round(t_arrival[(k, m, j)].x, 2),  # arrived
                                "-",  # prod start
                                "-",  # prod end
                                "-",  # left
                            )

                        else:
                            beekeeper_movements[(k, m)][i] = (
                                round(t_arrival[(k, m, i)].x, 2),
                                round(t_prod_start[(k, m, i)].x, 2),
                                round(t_prod_end[(k, m, i)].x, 2),
                                round(t_departure[(k, m, i)].x, 2),
                            )

    # Sort by order of arrival
    # TODO there's a way to merge beekeeper groups that travel the same route using sets
    print("\tLocation\t Arrived, Start, End, Depart")
    print("=" * 70)

    # Go through each group, sort by location times and print
    for group_id, group_path in beekeeper_movements.items():
        sorted_path = {k: v for k, v in sorted(group_path.items(), key=lambda item: item[1][0])}
        print(f"Beekeeper group {group_id[1]} from origin {group_id[0]}")

        for k, v in sorted_path.items():
            print("\t{}\t\t\t{}, {}, {}, {}".format(k, *v))

    return beekeeper_movements
