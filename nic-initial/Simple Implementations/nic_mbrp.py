import random
import pandas as pd
from gurobipy import *

from nic_helper import (
    Constants,
    read_data_instance,
    read_data_origins,
    read_data_parameters,
    distance,
)


def main():
    """Main execution for the model"""

    nectar_sources = "25"  # {'25', '50', '100'}
    instance = "01"  # {01, 02, ..., 10}

    data = read_data_instance(f"../data/{nectar_sources}-nectar-sources/instance_{instance}.txt")
    origins = read_data_origins(f"../data/{nectar_sources}-nectar-sources/origins.txt")
    parameters = read_data_parameters(f"../data/{nectar_sources}-nectar-sources/parameters.txt")

    # Constants
    transport_cost = int(parameters[Constants.PARAM_COST])
    transport_speed = int(parameters[Constants.PARAM_SPEED])
    fixed_transport_cost = int(parameters[Constants.PARAM_FIXED_COST])
    n_colonies = int(parameters[Constants.PARAM_N_COLONIES])

    n_nectar_varieties = 20

    # ----------------- Sets -----------------

    # Nectar source locations
    set_i = list(zip(data[Constants.INSTANCE_NECTAR_X], data[Constants.INSTANCE_NECTAR_Y]))
    n_nectar_locations = len(set_i)

    # Origin regions set
    set_origins = list(zip(origins[Constants.ORIGIN_X], origins[Constants.ORIGIN_Y]))
    n_origins = len(set_origins)
    set_k = list(range(len(origins)))
    # TODO need to respect that constraints reference only one origin at a time in unions, so need to come up with a
    #   way of getting all nectar locations and the i'th origin. For a single origin this is ignored
    set_origin_idx = list(range(n_origins))
    # set_origin_idx = list(range(n_nectar_locations, n_nectar_locations + n_origins))

    # Virtual terminal index, random out of any of the given nectar locations, assume there is only ever one
    terminus = [(6.0, 57.0)]  # this arrives to a solution fastest [random.choice(set_i)]
    n_terminal_locations = len(terminus)

    # Combined locations
    # (origin_locations, nectar_locations, terminus location)
    set_total_locations = set_origins + set_i + terminus
    set_total_idx = list(range(len(set_total_locations)))

    # Define a set of only nectar locations
    set_nectar_idx = set_total_idx[n_origins:-n_terminal_locations]
    set_nectar_origin_idx = set_total_idx[:-n_terminal_locations]

    # Define sets for the terminus location
    terminus_idx = set_total_idx[-1]
    set_nectar_terminus_idx = set_nectar_idx + set_total_idx[-n_terminal_locations:]

    # beekeeper groups from origin k
    # m[k][g], beekeeper group g leaving from origin k
    set_m = [list(range((int(i)))) for i in origins[Constants.ORIGIN_N_GROUP]]

    # Nectar varieties
    set_g = list(range(n_nectar_varieties))

    # Potential markets of honey products produced in nectar source i
    # U[i], potential markets for honey produced at i
    sources = data.iloc[:, -n_nectar_varieties:]
    set_u = [[] for _ in range(n_origins)] + \
            [[i + n_origins for i, val in enumerate(j) if val != 0] for _, j in sources.iterrows()] + [[]]

    # ----------------- Parameters -----------------

    # cap[i], Environmental carrying capacity of nectar source location i, represented by the maximal number of bee
    #  colonies that can survive and maintain a good productivity in that environment
    param_env_capacity = [0 for _ in range(n_origins)] + data[Constants.INSTANCE_ENV_CAPACITY].to_list() + [0]

    # yield[g][i], Average daily yield of honey products of variety g per colony at nectar source i
    # Assume that all types have the same yield at a location
    param_yield = [[] for _ in range(n_origins)] + \
                  [[i for _ in set_g] for i in data[Constants.INSTANCE_NECTAR_EFF].to_list()] + [[]]

    # start[i], Start of flowering time at nectar source i
    param_flower_start = [0 for _ in range(n_origins)] + data[Constants.INSTANCE_FLOWERING_START].to_list() + [0]

    # end[i], End of flowering time at nectar source i
    param_flower_end = [0 for _ in range(n_origins)] + data[Constants.INSTANCE_FLOWERING_END].to_list() + [0]

    # Transport distance from nectar source i to source j, applies to all locations
    # distance[i][j], where i is location from and j location to
    param_distance = [[distance(x1, x2) for x2 in set_total_locations] for x1 in set_total_locations]

    # Transport cost from nectar source i to source j, applies to all locations
    # c[i][j] = fixed_cost + unit_transport_cost * distance[i, j]
    param_cost = [[fixed_transport_cost + transport_cost * param_distance[i][j] for j in set_total_idx]
                  for i in set_total_idx]

    # Unit price of honey products produced from nectar variety g at market, applies to nectar sources only
    # price[i][g], where i is location and g is nectar type
    param_price = [[0] for _ in range(n_origins)] + \
                  [[sources.iloc[i - n_origins, g] for g in set_g] for i in set_nectar_idx] + [[0]]

    # Average number of bee colonies from each group
    param_bee_avg = origins[Constants.ORIGIN_N_GROUP].mean()

    # A reasonably large number
    # Appendix A2: L in constraint 37 can be max(time_end[i] - time_start[i] + ceil(d[i, j]/v), 1)
    param_l = 999

    # ----------------- Model -----------------
    model = Model("MBRP")

    # ----------------- Variables -----------------

    # X[k, m, i, j] -> {0, 1}, equals 1 if beekeeper group m from origin k depart i to j
    # Note that this includes origin regions -> [origin_locations, nectar locations, terminal locations]
    x = {
        (k, m, i, j): model.addVar(vtype=GRB.BINARY)
        for k in set_k
        for m in set_m[k]
        for i in set_nectar_origin_idx
        for j in set_nectar_terminus_idx
    }

    # Z[k, m, i, j] -> {0, 1}, indicates whether honey products produced at location i will be sold at
    # location j for beekeeper group m from origin region k
    z = {
        (k, m, i, j): model.addVar(vtype=GRB.BINARY)
        for k in set_k
        for m in set_m[k]
        for i in set_nectar_idx
        for j in set_nectar_idx
    }

    # t[k, m, i] -> float, arrival time of beekeeper group m from origin region k at nectar source location i
    t_arrival = {(k, m, i): model.addVar(lb=0) for k in set_k for m in set_m[k] for i in set_nectar_terminus_idx}

    # t[k, m, i] -> float, production start time of beekeeper group m from origin region k at nectar source location i
    t_prod_start = {(k, m, i): model.addVar() for k in set_k for m in set_m[k] for i in set_nectar_idx}

    # t[k, m, i] -> float, production time end of beekeeper group m from origin region k at nectar source location i
    t_prod_end = {(k, m, i): model.addVar() for k in set_k for m in set_m[k] for i in set_nectar_idx}

    # t[k, m, i] -> float, departure time of beekeeper group m from origin region k at nectar source location i
    t_departure = {(k, m, i): model.addVar() for k in set_k for m in set_m[k] for i in set_nectar_origin_idx}

    # ----------------- Objective -----------------

    # # Debugging
    # for k in set_k:
    #     for m in set_m[k]:
    #         for i in set_nectar_idx:
    #             for g in set_g:
    #                 try:
    #                     print(param_yield[i][g])
    #                     print(t_prod_end[(k, m, i)])
    #                     print(t_prod_start[(k, m, i)])
    #                     print([param_price[j][g] for j in set_u[i]])
    #                     print([z[(k, m, i, j)] for j in set_u[i]])
    #                 except IndexError:
    #                     print("Error on", k, m, i, g)
    #                     break

    # 1. the trade-off between the total income from production and the total cost of migration
    model.setObjective(
        param_bee_avg * quicksum(param_yield[i][g] * (t_prod_end[(k, m, i)] - t_prod_start[(k, m, i)]) *
                                 quicksum(param_price[j][g] * z[(k, m, i, j)] for j in set_u[i])
                                 for k in set_k for m in set_m[k] for i in set_nectar_idx for g in set_g)
        - quicksum(param_cost[i][j] * x[(k, m, i, j)] for k in set_k for m in set_m[k]
                   for i in set_nectar_origin_idx for j in set_nectar_terminus_idx),
        GRB.MAXIMIZE
    )

    # ----------------- Constraints -----------------

    # 2 & 3. each beekeeper group must depart from his or her origin region and arrive at the virtual terminus. Assumes
    # there only exists one terminus. i in this equation references the x value of the origin k + n_nectar_locations
    # Since set_k as origins indexed from 0 to n_origins, x_i is similarly indexed by k
    depart_from_origin = {
        (k, m): model.addConstr(quicksum(x[(k, m, k, j)] for j in set_nectar_idx) == 1)
        for k in set_k for m in set_m[k]
    }

    arrive_at_terminus = {
        (k, m): model.addConstr(quicksum(x[(k, m, i, terminus_idx)] for i in set_nectar_idx) == 1)
        for k in set_k for m in set_m[k]
    }

    # 4. flow conservation at each nectar source location
    flow_conservation = {
        (k, m, j): model.addConstr(quicksum(x[(k, m, i, j)] for i in set_nectar_origin_idx) ==
                                   quicksum(x[(k, m, j, i)] for i in set_nectar_terminus_idx))
        for k in set_k for m in set_m[k] for j in set_nectar_idx
    }

    flow_conservation_2 = {
        (k, m, j): model.addConstr(quicksum(x[(k, m, j, i)] for i in set_nectar_terminus_idx) <= 1)
        for k in set_k for m in set_m[k] for j in set_nectar_idx
    }

    # 5. forces z[k, m, i, j] = 0 if market j is not visited by a corresponding group
    not_visited = {
        (k, m, i, j): model.addConstr(z[(k, m, i, j)] <= quicksum(x[(k, m, ii, j)] for ii in set_nectar_origin_idx))
        for k in set_k for m in set_m[k] for i in set_nectar_idx for j in set_u[i]
    }

    # 6. requires the balance between production and sales
    balance_product_and_sales = {
        (k, m, i): model.addConstr(quicksum(z[(k, m, i, j)] for j in set_u[i]) <= 1)
        for k in set_k for m in set_m[k] for i in set_nectar_idx
    }

    # 7. limit the environmental carrying capacity of each nectar source location, where the sign function sgn()
    # guarantees that a nectar source will not be occupied by any beekeeper group that only passes through there
    # without production (i.e., the production time of such a group is zero)
    def sgn(xx):
        return xx  # TODO implement this function

    env_capacity_limit = {
        j: model.addConstr(param_bee_avg * quicksum(
            sgn(t_prod_end[(k, m, j)] - t_prod_start[(k, m, j)]) for k in set_k for m in set_m[k]
        ) <= param_env_capacity[j]) for j in set_nectar_idx
    }

    # 8. Forces z[k, m, i, j] = 0 if market j is accessed earlier than nectar source i
    order_of_access = {
        (k, m, i, j): model.addConstr(t_prod_end[(k, m, i)] - t_prod_end[(k, m, j)] <= param_l * (1 - z[(k, m, i, j)]))
        for k in set_k for m in set_m[k] for i in set_nectar_idx for j in set_u[i]
    }

    # 9. Denotes that t_prod_start[i] and t_prod_end[i] should be equal if nectar source i is not visited
    # (i.e., i has no yield)
    source_not_visited = {
        (k, m, i): model.addConstr(t_prod_end[(k, m, i)] - t_prod_start[(k, m, i)] <=
                                   param_l * quicksum(x[(k, m, i, j)] for j in set_nectar_terminus_idx))
        for k in set_k for m in set_m[k] for i in set_nectar_idx
    }

    # 10. Express the time relationship among sequentially accessed locations, where the ceiling function returns the
    # minimal integer number that is greater than or equal to the input parameter
    sequential_time = {
        (k, m, i, j): model.addConstr(
            t_departure[(k, m, i)] + math.ceil(param_distance[i][j] / transport_speed) -
            param_l * (1 - x[(k, m, i, j)]) <= t_arrival[(k, m, j)])
        for k in set_k for m in set_m[k] for i in set_nectar_origin_idx for j in set_nectar_terminus_idx if i != j
    }

    # 11 - 13. All related to the timing rules illustrated in figure 2 of the paper
    # TODO is there a more efficient way to combine these inequalities?
    operations_order_0 = {
        (k, m, i): model.addConstr(t_arrival[(k, m, i)] <= t_prod_start[(k, m, i)])
        for k in set_k for m in set_m[k] for i in set_nectar_idx
    }

    operations_order_1 = {
        (k, m, i): model.addConstr(t_prod_start[(k, m, i)] <= t_prod_end[(k, m, i)])
        for k in set_k for m in set_m[k] for i in set_nectar_idx
    }

    operations_order_2 = {
        (k, m, i): model.addConstr(t_prod_end[(k, m, i)] <= t_departure[(k, m, i)])
        for k in set_k for m in set_m[k] for i in set_nectar_idx
    }

    start_after_flowering_open = {
        (k, m, i): model.addConstr(param_flower_start[i] <= t_prod_start[(k, m, i)])
        for k in set_k for m in set_m[k] for i in set_nectar_idx
    }

    end_before_flowering_close = {
        (k, m, i): model.addConstr(t_prod_end[(k, m, i)] <= param_flower_end[i])
        for k in set_k for m in set_m[k] for i in set_nectar_idx
    }

    # 14 - 16. definition of timing decision variables, routing decision variables, and remote
    # sales decision variables. These are handled in variable definitions

    #
    # # Custom  # FIXME this should be handled in conservation of flow
    # # Can't move to own location
    # must_move_onwards = {
    #     (k, m, i): model.addConstr(x[(k, m, i, i)] == 0)
    #     for k in set_k for m in set_m[k] for i in set_nectar_idx
    # }

    # ----------------- Run model -----------------
    model.optimize()

    # ----------------- Results interpretation -----------------

    # X[k, m, i, j] -> {0, 1}, equals 1 if beekeeper group m from origin k depart i to j
    # Z[k, m, i, j] -> {0, 1}, indicates whether honey products produced at location i will be sold at location j for
    #   beekeeper group m from origin region k
    # t_arrival[k, m, i] -> float, arrival time of group m from origin region k at nectar source location i
    # t_prod_start[k, m, i] -> float, production start time of group m from origin region k at nectar source location i
    # t_prod_end[k, m, i] -> float, production time end of group m from origin region k at nectar source location i
    # t_departure[k, m, i] -> float, departure time of group m from origin region k at nectar source location i

    # Interpret results in line with how the original paper does
    beekeeper_movements = {(k, m): {} for k in set_k for m in set_m[k]}
    for k in set_k:
        for m in set_m[k]:

            # For beekeeper group m from origin k
            for i in set_nectar_origin_idx:
                for j in set_nectar_terminus_idx:

                    if x[(k, m, i, j)].x > 0.9:
                        # Beekeeper group m from origin k moves from location i to j
                        # Record arrivals, departures, and production times for location i

                        if i in set_origin_idx:
                            beekeeper_movements[(k, m)][(i, j)] = (
                                0.0,  # arrived
                                0.0,  # prod start
                                0.0,  # prod end
                                round(t_departure[(k, m, i)].x, 2),  # left
                            )

                        elif j == terminus_idx:
                            beekeeper_movements[(k, m)][(i, j)] = (
                                round(t_arrival[(k, m, i)].x, 2),  # arrived
                                0.0,  # prod start
                                0.0,  # prod end
                                365,  # left
                            )

                        else:
                            beekeeper_movements[(k, m)][(i, j)] = (
                                round(t_arrival[(k, m, i)].x, 2),
                                round(t_prod_start[(k, m, i)].x, 2),
                                round(t_prod_end[(k, m, i)].x, 2),
                                round(t_departure[(k, m, i)].x, 2),
                            )

    # Sort by order of arrival
    # TODO there's a way to merge beekeeper groups that travel the same route using sets
    print(f"Latest flowering end: {data[Constants.INSTANCE_FLOWERING_END].max()}\n")
    print("\tLocation\t\t Arrived, Start, End, Depart")
    print("=" * 70)

    # Go through each group, sort by location times and print
    for group_id, group_path in beekeeper_movements.items():
        sorted_path = {k: v for k, v in sorted(group_path.items(), key=lambda item: item[1])}
        print(f"Beekeeper group {group_id[1]} from origin {group_id[0]}")

        for k, v in sorted_path.items():
            k_ = f"\t{k[0]} -> {k[1]}"
            print(k_, " " * (10 - len(k_)), f"\t\t {v}")

    print("Debug line")


if __name__ == '__main__':
    main()
