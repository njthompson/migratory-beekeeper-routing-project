import math
from typing import Tuple

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd


class Constants:
    INSTANCE_ID = "ID"
    INSTANCE_NECTAR_X = "X"
    INSTANCE_NECTAR_Y = "Y"
    INSTANCE_NECTAR_EFF = "Eff."
    INSTANCE_ENV_CAPACITY = "Cap."
    INSTANCE_NECTAR_CATEGORY = "Cate."
    INSTANCE_FLOWERING_START = "S."
    INSTANCE_FLOWERING_END = "E."

    ORIGIN_ID = "ID"
    ORIGIN_X = "X"
    ORIGIN_Y = "Y"
    ORIGIN_N_GROUP = "GroupNum"

    PARAM_COST = "Cost"
    PARAM_SPEED = "Speed"
    PARAM_FIXED_COST = "fixedCost"
    PARAM_N_COLONIES = "b"


class MbrpData:

    def __init__(self,
                 transport_cost,
                 transport_speed,
                 fixed_transport_cost,
                 n_colonies,
                 set_i,
                 n_nectar_locations,
                 n_origins,
                 terminus,
                 n_terminal_locations,
                 set_total_locations,
                 set_total_idx,
                 set_origin_idx,
                 set_nectar_idx,
                 terminus_idx,
                 set_nectar_terminus_idx,
                 set_m,
                 set_g,
                 sources,
                 set_u,
                 set_k,
                 param_env_capacity,
                 param_yield,
                 non_sales_param_yield,
                 param_flower_start,
                 param_flower_end,
                 param_distance,
                 param_nectar_distance,
                 param_cost,
                 param_price,
                 non_sales_param_price,
                 param_bee_avg,
                 param_l,
                 ):

        self.transport_cost = transport_cost
        self.transport_speed = transport_speed
        self.fixed_transport_cost = fixed_transport_cost

        self.n_colonies = n_colonies
        self.n_nectar_locations = n_nectar_locations
        self.n_terminal_locations = n_terminal_locations
        self.n_origins = n_origins

        self.terminus = terminus
        self.terminus_idx = terminus_idx

        self.set_i = set_i
        self.set_total_locations = set_total_locations
        self.set_total_idx = set_total_idx
        self.set_origin_idx = set_origin_idx
        self.set_nectar_idx = set_nectar_idx
        self.set_nectar_terminus_idx = set_nectar_terminus_idx
        self.set_m = set_m
        self.set_g = set_g
        self.set_u = set_u
        self.set_k = set_k

        self.sources = sources

        self.param_env_capacity = param_env_capacity
        self.param_yield = param_yield
        self.non_sales_param_yield = non_sales_param_yield
        self.param_flower_start = param_flower_start
        self.param_flower_end = param_flower_end
        self.param_distance = param_distance
        self.param_nectar_distance = param_nectar_distance
        self.param_cost = param_cost
        self.param_price = param_price
        self.non_sales_param_price = non_sales_param_price
        self.param_bee_avg = param_bee_avg
        self.param_l = param_l

        self.adjacency_matrix, self.shortest_path = self.create_adjacency_matrix()
        self.time_start_matrix, self.time_end_matrix = self.create_time_matrices()
        self.time_start_matrix_2, self.time_end_matrix_2 = self.create_time_matrices_2()

    def create_adjacency_matrix(self):
        """Create the adjacency matrix described in 3.3.1 of the paper"""

        # Find time matrix T[i, j], representing the shortest time from i to j using Floyd shortest path
        # Assume that distance matrix is square
        n = len(self.set_total_locations)
        shortest_path = {(i, j): self.param_distance[i][j] for i in range(n) for j in range(n)}

        for k in range(n):
            for i in range(n):
                for j in range(n):
                    # If vertex k is the shortest path from i to j then update the existing value
                    shortest_path[i, j] = min(shortest_path[i, j], shortest_path[i, k] + shortest_path[k, j])

        # speed = distance / time -> time = distance / speed
        # Can do this separately since operation is uniform
        shortest_path = {k: v / self.transport_speed for k, v in shortest_path.items()}

        # Create adjacency matrix, gives a one or zero whether beekeper can go from i to j within flowering period
        adjacency = [[0 for i in range(n)] for j in range(n)]
        for i in range(n):
            for j in range(n):
                if i in self.set_k or j == self.terminus_idx:
                    adjacency[i][j] = 1
                else:
                    adjacency[i][j] = int(self.param_flower_start[i] + shortest_path[i, j] <= self.param_flower_end[j])

        return adjacency, shortest_path

    def create_time_matrices(self):
        """
        Create the time matrices that are used in 3.3.1 from the paper. Based on theorem 1 that in the non-sales
        MBRP, it is a sufficient but not necessary condition that beekeepers stay as long as possible at one source with
        better revenue if nectar source j is accessible to i.

        Base these off corollary 1 and 2
        """

        n = self.n_nectar_locations
        p = self.non_sales_param_price
        q = self.non_sales_param_yield

        new_time_start = [np.nan for _ in range(n)]
        new_time_end = [np.nan for _ in range(n)]

        # Iterate through i and j to find values for time start and time end
        for i in range(n):
            for j in range(n):

                # Only applies if sources are adjacent
                if self.adjacency_matrix[i][j]:

                    if p[i] * q[i] >= p[j] * q[j]:
                        new_time_end[i] = self.param_flower_end[i]
                        new_time_start[j] = max(new_time_end[i] + self.shortest_path[i, j], self.param_flower_start[j])

                    elif p[i] * q[i] < p[j] * q[j]:
                        new_time_start[j] = self.param_flower_start[j]
                        new_time_end[i] = min(new_time_start[j] - self.shortest_path[i, j], new_time_end[i])

                    else:
                        raise ValueError(f"Inequality not satisfied: {i, j, p[i], q[i], p[j], q[j]}")

                # print(new_time_start[j], new_time_end[i])

        return new_time_start, new_time_end

    def create_time_matrices_2(self):
        """
        Create the time matrices that are used in 3.3.1 from the paper. Based on theorem 1 that in the non-sales
        MBRP, it is a sufficient but not necessary condition that beekeepers stay as long as possible at one source with
        better revenue if nectar source j is accessible to i.

        Base these off corollary 1 and 2
        """

        n = self.n_nectar_locations
        p = self.non_sales_param_price
        q = self.non_sales_param_yield

        # time_start[j][i], time start i when coming from j
        new_time_start = [[np.nan for _ in range(n)] for _ in range(n)]

        # time_end[i][j], time end i when heading to j
        new_time_end = [[np.nan for _ in range(n)] for _ in range(n)]

        # Iterate through i and j to find values for time start and time end
        for i in range(n):
            for j in range(n):

                # Only applies if sources are adjacent
                if self.adjacency_matrix[i][j]:

                    if p[i] * q[i] >= p[j] * q[j]:
                        new_time_end[i][j] = self.param_flower_end[i]
                        new_time_start[i][j] = \
                            max(new_time_end[i][j] + self.shortest_path[i, j], self.param_flower_start[j])

                    elif p[i] * q[i] < p[j] * q[j]:
                        new_time_start[i][j] = self.param_flower_start[j]
                        new_time_end[i][j] = \
                            min(new_time_start[i][j] - self.shortest_path[i, j], new_time_end[i][j])

                    else:
                        raise ValueError(f"Inequality not satisfied: {i, j, p[i], q[i], p[j], q[j]}")

        def test(path):
            """Test a path to see if it makes sense"""
            # TODO add in distances
            print(f"New test for path: {path}")
            p0, p1, p2 = path

            # Make sure that the test is applicable
            assert self.adjacency_matrix[p0][p1] == 1, f"{p0} and {p1} are not adjacent"
            assert self.adjacency_matrix[p1][p2] == 1, f"{p1} and {p2} are not adjacent"

            print(f"Leave {p0} at: {new_time_end[p0][p1]}")
            print(f"Get to {p1} at: {new_time_start[p0][p1]}")
            print(f"Leave {p1} at: {new_time_end[p1][p2]}")
            print(f"Get to {p2} at: {new_time_start[p1][p2]}\n\n")

        # # Test 1
        # test((0, 2, 4))
        # # Test 2
        # test((1, 3, 4))

        return new_time_start, new_time_end


def get_problem_data(nectar_sources: str,
                     instance: str,
                     terminus: tuple = (6.0, 57.0),
                     plot: bool = True,
                     l_: float = 999) -> MbrpData:
    """Read problem data from csv files and parse this into problem inputs"""

    data_raw = read_data_instance(f"../data/{nectar_sources}-nectar-sources/instance_{instance}.txt")
    origins = read_data_origins(f"../data/{nectar_sources}-nectar-sources/origins.txt")
    parameters = read_data_parameters(f"../data/{nectar_sources}-nectar-sources/parameters.txt")

    # Constants
    transport_cost = int(parameters[Constants.PARAM_COST])
    transport_speed = int(parameters[Constants.PARAM_SPEED])
    fixed_transport_cost = int(parameters[Constants.PARAM_FIXED_COST])
    n_colonies = int(parameters[Constants.PARAM_N_COLONIES])

    n_nectar_varieties = 20

    # ----------------- Sets -----------------

    # Nectar source locations
    set_i = list(zip(data_raw[Constants.INSTANCE_NECTAR_X], data_raw[Constants.INSTANCE_NECTAR_Y]))
    n_nectar_locations = len(set_i)

    # Origin regions set
    set_origins = list(zip(origins[Constants.ORIGIN_X], origins[Constants.ORIGIN_Y]))
    n_origins = len(set_origins)
    set_k = list(range(len(origins)))
    set_origin_idx = list(range(n_origins))

    # Virtual terminal index, random out of any of the given nectar locations, assume there is only ever one
    n_terminal_locations = 1

    # Combined locations
    # (origin_locations, nectar_locations, terminus location)
    set_total_locations = set_origins + set_i + [terminus]
    set_total_idx = list(range(len(set_total_locations)))

    # Define a set of only nectar locations
    set_nectar_idx = set_total_idx[n_origins:-n_terminal_locations]

    # Define sets for the terminus location
    terminus_idx = set_total_idx[-1]
    set_nectar_terminus_idx = set_nectar_idx + [terminus_idx]

    # beekeeper groups from origin k
    # m[k][g], beekeeper group g leaving from origin k
    set_m = [list(range((int(i)))) for i in origins[Constants.ORIGIN_N_GROUP]]

    # Nectar varieties
    set_g = list(range(n_nectar_varieties))

    # Potential markets of honey products produced in nectar source i
    # U[i], potential markets for honey produced at i
    sources = data_raw.iloc[:, -n_nectar_varieties:]
    set_u = [[] for _ in range(n_origins)] + \
            [[i + n_origins for i, val in enumerate(j) if val != 0] for _, j in sources.iterrows()] + [[]]

    # ----------------- Parameters -----------------

    # cap[i], Environmental carrying capacity of nectar source location i, represented by the maximal number of bee
    #  colonies that can survive and maintain a good productivity in that environment
    param_env_capacity = [0 for _ in range(n_origins)] + data_raw[Constants.INSTANCE_ENV_CAPACITY].to_list() + [0]

    # yield[g][i], Average daily yield of honey products of variety g per colony at nectar source i
    # Assume that all types have the same yield at a location
    param_yield = [[] for _ in range(n_origins)] + \
                  [[i for _ in set_g] for i in data_raw[Constants.INSTANCE_NECTAR_EFF].to_list()] + [[]]
    non_sales_param_yield = data_raw[Constants.INSTANCE_NECTAR_EFF].to_list()

    # start[i], Start of flowering time at nectar source i
    param_flower_start = [0 for _ in range(n_origins)] + data_raw[Constants.INSTANCE_FLOWERING_START].to_list() + [0]

    # end[i], End of flowering time at nectar source i
    param_flower_end = [0 for _ in range(n_origins)] + data_raw[Constants.INSTANCE_FLOWERING_END].to_list() + [0]

    # Transport distance from nectar source i to source j, applies to all locations
    # distance[i][j], where i is location from and j location to
    param_distance = [[distance(x1, x2) for x2 in set_total_locations] for x1 in set_total_locations]
    param_nectar_distance = [[distance(x1, x2) for x2 in set_i] for x1 in set_i]

    # Transport cost from nectar source i to source j, applies to all locations
    # c[i][j] = fixed_cost + unit_transport_cost * distance[i, j]
    cost_factor = 1
    param_cost = [[cost_factor * (fixed_transport_cost + transport_cost * param_distance[i][j]) for j in set_total_idx]
                  for i in set_total_idx]

    # Unit price of honey products produced from nectar variety g at market, applies to nectar sources only
    # price[i][g], where i is location and g is nectar type
    param_price = [[0] for _ in range(n_origins)] + \
                  [[sources.iloc[i - n_origins, g] for g in set_g] for i in set_nectar_idx] + [[0]]
    non_sales_param_price = sources.replace(0, np.nan).max(axis=1).to_list()

    # Average number of bee colonies from each group
    param_bee_avg = origins[Constants.ORIGIN_N_GROUP].mean()

    # A reasonably large number
    # Appendix A2: L in constraint 37 can be max(time_end[i] - time_start[i] + ceil(d[i, j]/v), 1)
    param_l = l_

    if plot:
        sources_x = [i[0] for i in set_i]
        sources_y = [i[1] for i in set_i]
        origins_x = [i[0] for i in set_origins]
        origins_y = [i[1] for i in set_origins]
        plt.scatter(sources_x, sources_y, c="blue")
        plt.scatter(origins_x, origins_y, c="red")
        plt.scatter(terminus[0], terminus[1], c="k")
        plt.show()

    return MbrpData(
        transport_cost=transport_cost,
        transport_speed=transport_speed,
        fixed_transport_cost=fixed_transport_cost,
        n_colonies=n_colonies,
        set_i=set_i,
        n_origins=n_origins,
        n_nectar_locations=n_nectar_locations,
        terminus=terminus,
        n_terminal_locations=n_terminal_locations,
        set_total_locations=set_total_locations,
        set_total_idx=set_total_idx,
        set_nectar_idx=set_nectar_idx,
        set_origin_idx=set_origin_idx,
        terminus_idx=terminus_idx,
        set_nectar_terminus_idx=set_nectar_terminus_idx,
        set_m=set_m,
        set_g=set_g,
        sources=sources,
        set_u=set_u,
        set_k=set_k,
        param_env_capacity=param_env_capacity,
        param_yield=param_yield,
        non_sales_param_yield=non_sales_param_yield,
        param_flower_start=param_flower_start,
        param_flower_end=param_flower_end,
        param_distance=param_distance,
        param_nectar_distance=param_nectar_distance,
        param_cost=param_cost,
        param_price=param_price,
        non_sales_param_price=non_sales_param_price,
        param_bee_avg=param_bee_avg,
        param_l=param_l,
    )


def read_data_instance(file: str) -> pd.DataFrame:
    """Read one of the generated data instances supplied by the paper"""

    f = open(file, "r")
    contents = [i.split("\t") for i in f.read().split("\n")]
    df = pd.DataFrame(data=contents[1:], columns=contents[0])

    # Remove last column, which is always empty, and cast to float
    df = df.iloc[:, :-1].astype(float)
    return df


def read_data_origins(file: str) -> pd.DataFrame:
    """Read origins.txt from specified location"""

    f = open(file, "r")
    contents = [i.split("\t") for i in f.read().split("\n")]

    df = pd.DataFrame(data=contents[2:], columns=contents[1])
    return df.astype(float)


def read_data_parameters(file: str) -> dict:
    """Read parameters.txt from specified location, return dictionary of params"""

    f = open(file, "r")
    contents = [i.split("\t") for i in f.read().split("\n")]
    return dict(zip(contents[0], contents[1]))


def distance(x1: Tuple, x2: Tuple) -> float:
    """Return the euclidean distance from x1 to x2"""
    return int(math.hypot(x1[0] - x2[0], x1[1] - x2[1]) + 0.5)
