"""
        while True:
            rmp.optimize()
            try:
                print('Obj val:', rmp.objVal)
            except AttributeError:
                print("RMP failed")

            changed = False
            for k in data.set_k:
                for m in data.set_m[k]:

                    # Solve k'th sub-problem
                    pi_0 = [i.pi for i in constraint_groups_from_origin.values()]
                    pi_1 = [i.pi for i in constraint_env_capacity.values()]
                    solution = solve_pricing_sub_problem(data=data, k=k, m=m, pi_0=pi_0, pi_1=pi_1, verbose=0)

                    if solution.total_revenue > EPSILON:

                        # Check that column doesn't already exist
                        if (k, tuple(solution.visits)) in z:
                            # TODO make this an error
                            # raise ValueError(f"Duplicate column: {k} / {solution.visits}")
                            continue

                        # Ensure the loop won't finish on this iteration
                        changed = True

                        # Calculate route capacity, ignoring the initial and final locations
                        route_capacity = min(data.param_env_capacity[i] for i in solution.visits[1:-1]) + 1

                        # Save the new route to the model
                        all_valid_routes[k, tuple(solution.visits)] = solution.total_revenue, route_capacity
                        all_valid_beekeeper_routes[
                            k, m, tuple(solution.visits)] = solution.total_revenue, route_capacity
                        route_map[tuple(solution.visits)] = solution

                        z[k, tuple(solution.visits)] = new_var = rmp.addVar(obj=solution.total_revenue)

                        x_gamma = {
                            (k, m, i, j, r): 1 if (i, j) in get_trip_arcs(r) else 0
                            for k in data.set_k
                            for m in data.set_m[k]
                            for i in data.set_total_idx
                            for j in data.set_total_idx
                            for r_k, r_m, r in all_valid_beekeeper_routes
                            if k == r_k and m == r_m
                        }

                        # Fix cover constraint
                        rmp.chgCoeff(constraint_groups_from_origin[k], new_var, 1)

                        # Fix environment constraint
                        r = tuple(solution.visits)
                        for j in solution.visits[1:-1]:
                            rmp.chgCoeff(constraint_env_capacity[j], new_var,
                                         sum(x_gamma[k, m, i, j, r] *
                                             indicator(solution, j)
                                             for mm in data.set_m[k]
                                             for i in [k] + data.set_nectar_idx
                                             if m == mm))

                        print(f"Added column: {k, tuple(solution.visits)}")

                        # TODO Maybe try redefining objective and constraints with new data?
                    else:
                        print("Negative cost")

            if not changed:
                break
"""