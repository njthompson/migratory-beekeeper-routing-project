\contentsline {section}{\numberline {1}Michaels Advice}{2}% 
\contentsline {section}{\numberline {2}Introduction}{2}% 
\contentsline {section}{\numberline {3}Paper Summary}{3}% 
\contentsline {section}{\numberline {4}Methodology}{4}% 
\contentsline {subsection}{\numberline {4.1}Problem Formulation}{4}% 
\contentsline {subsubsection}{\numberline {4.1.1}The Restricted Master Problem}{6}% 
\contentsline {subsubsection}{\numberline {4.1.2}The Pricing Subproblems}{6}% 
\contentsline {subsection}{\numberline {4.2}Solution Implementation}{7}% 
\contentsline {subsubsection}{\numberline {4.2.1}Algorithm 1 - Greedy Route Initialisation (Appendix A.4)}{8}% 
\contentsline {subsubsection}{\numberline {4.2.2}Algorithm 2 - Framework for suboptimal solution search (A.5)}{8}% 
\contentsline {subsubsection}{\numberline {4.2.3}Algorithm 4 - Revise labelling algorithm (A.7)}{10}% 
\contentsline {subsubsection}{\numberline {4.2.4}Algorithm 5 - Algorithm for appending a new nectar source (A.8)}{10}% 
\contentsline {subsubsection}{\numberline {4.2.5}Algorithm 3 - The CG algorithm for the MBRP (A.6)}{12}% 
\contentsline {section}{\numberline {5}Project Results}{12}% 
\contentsline {section}{\numberline {6}Next Steps}{13}% 
\contentsline {section}{\numberline {7}Future of the MBRP}{13}% 
\contentsline {section}{\numberline {8}Conclusion}{13}% 
