The following folders contain
(i) the program files of instance generation, 
(ii) the generated instances, 
(iii) the results of instance computation, and 
(iv) the data of distance matrix of case study
of our study on: 

The Migratory Beekeeping Routing Problem: Model and an Exact Algorithm



1- "InstanceGenerationProgram" folder:

The program is written in C# language and by using the Math.Net library, in which the distribution parameters can be found Clusters.cs.



2- "instances" folder:

The instances with different scales (25, 50, 100 nectar sources) are provided. In each subfolder, ten instances with the following columns are listed.

ID: the ID numbers of nectar resources;
X, Y: the coordination;
Eff.: the production efficiency (average daily yield of the honey product);
Cap.: the environment carrying capacity;
Cate.: the nectar source category;
S., E.: start and end of flowering time;
P[0] - P[19]: the prices that correspond to the different honey categories (varying from category 0 to 19).



Additionally, parameters to the origin regions are shown in “origins.txt”, where the column GroupNum denotes the number of groups departing from the corresponding origin.



Other parameters are listed in “parameters.txt”.

Cost	: unit cost of transportation;
Speed: speed of transportation;
fixedCost: the fixed cost when migrating form one nectar source to another;
b: number of bee colonies from each group.



3- "results" folder:

The objective values and corresponding routes are presented. 

The labels are formatted as identification (ID) (starting production time, ending production time, leaving time, and sales location ID). The column N represents the number of groups that follow the corresponding route.

Note that the ID of a nectar source location in the results is equal to the number of selected origin regions plus the source’s own ID.



4- “Distance Matrix of Case Study” folder:

Distance between nectar sources in different provinces of China are measured approximately by the distance between the corresponding provincial capitals.
